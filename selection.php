<?php include './partials/_head.php'; ?>

<div class="app__page app__page--selection selection">

  <header class="selection__header header header--white">
    <?php include './partials/header--white.php'; ?>
  </header>

  <div class="selection__blocks">

    <div class="selection__block">
      <div class="selection__media media">
        <img alt="" class="media__image display__image" loading="lazy"
          src="./media/selection__media.png" />
      </div>
      <div class="selection__visual">
        <?php $heading = 'PASAKA'; include './partials/scenes/selection__scene--1.php'; ?>
      </div>
      <a class="selection__trigger" href="index"></a>
    </div>

    <div class="selection__block">
      <div class="selection__media media">
        <img alt="" class="media__image display__image" loading="lazy"
          src="./media/selection__media.png" />
      </div>
      <div class="selection__visual">
        <?php $heading = 'JŪRA'; include './partials/scenes/selection__scene--2.php'; ?>
      </div>
      <a class="selection__trigger" href="index"></a>
    </div>

    <div class="selection__block">
      <div class="selection__media media">
        <img alt="" class="media__image display__image" loading="lazy"
          src="./media/selection__media.png" />
      </div>
      <div class="selection__visual">
        <?php $heading = 'PASAKA'; include './partials/scenes/selection__scene--1.php'; ?>
      </div>
      <a class="selection__trigger" href="index"></a>
    </div>

    <div class="selection__block">
      <div class="selection__media media">
        <img alt="" class="media__image" loading="lazy" src="./media/selection__media.png" />
      </div>
      <div class="selection__visual">
        <?php $heading = 'JŪRA'; include './partials/scenes/selection__scene--2.php'; ?>
      </div>
      <a class="selection__trigger" href="index"></a>
    </div>

  </div>

</div>

<?php include './partials/_foot.php';
