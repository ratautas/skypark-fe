<?php include './partials/_head.php'; ?>

<div class="app__page app__page--default default">

  <div class="default__intro">

    <header class="default__header header header--white">
      <?php include './partials/header--white.php'; ?>
    </header>

    <?php $heading = 'ARRIVING FOR THE<br>FIRST TIME'; include './partials/scenes/default__scene--2.php'; ?>

  </div>

  <main class="default__content">

    <header class="default__header header header--black">
      <?php include './partials/header--black.php'; ?>
    </header>

    <section class="default__section default__section--wysiwyg_before-tween" data-tweener>
      <div class="default__container default__container--wysiwyg container">
        <div class="default__wysiwyg _wysiwyg">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodifier tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea commodifiero consequat. Duis aute
            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur.</p>
          <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
            mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit
            voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
            inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
          <p>&nbsp;</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodifier tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea commodifiero consequat. Duis aute
            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur.</p>
          <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
            mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit
            voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
            inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
        </div>
      </div>
    </section>

  </main>

</div>

<?php include './partials/_foot.php';
