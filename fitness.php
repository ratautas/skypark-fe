<?php include './partials/_head.php'; ?>

<div class="app__page app__page--default default">

  <div class="default__intro">

    <header class="default__header header header--white">
      <?php include './partials/header--white.php'; ?>
    </header>

    <?php $heading = 'FITNESS<br>TRAINING'; include './partials/scenes/default__scene--2.php'; ?>

  </div>

  <main class="default__content">

    <header class="default__header header header--black">
      <?php include './partials/header--black.php'; ?>
    </header>

    <section class="default__section default__section--wysiwyg _before-tween" data-tweener>
      <div class="default__container default__container--wysiwyg container">
        <div class="default__wysiwyg _wysiwyg">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodifier tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea commodifiero consequat. Duis aute
            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur.</p>
          <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
            mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit
            voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
            inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
        </div>
      </div>
    </section>

    <section class="default__section default__section--quick-nav">
      <div class="default__container default__container--quick-nav container">
        <div class="default__quick-nav quick-nav">
          <div class="quick-nav__list">
            <div class="quick-nav__item underlink _before-tween" data-tweener
              data-modal-trigger="fitness">BOOK FITNESS
              TRAININGS</div>
          </div>
        </div>
      </div>
    </section>

    <section class="default__section default__section--display">
      <div class="default__container container container--l">
        <div class="default__display display">
          <div class="display__blocks">
            <div class="display__media display__media--1 media _before-tween" data-tweener>
              <img alt="" class="media__image display__image display__image--1" loading="lazy"
                src="./media/safety--2.png" data-parallax="70" />
            </div>
            <div class="display__media display__media--2 media _before-tween" data-tweener>
              <img alt="" class="media__image display__image display__image--2" loading="lazy"
                src="./media/safety--1.png" data-parallax="-50" />
            </div>
            <div class="display__media display__media--3 media _before-tween" data-tweener>
              <img alt="" class="media__image display__image display__image--3" loading="lazy"
                src="./media/safety--3.png" data-parallax="-30" />
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="default__section default__section--wysiwyg _before-tween" data-tweener>
      <div class="default__container default__container--wysiwyg container">
        <div class="default__wysiwyg _wysiwyg">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodifier tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea commodifiero consequat. Duis aute
            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur.</p>
        </div>
      </div>
    </section>

  </main>

</div>

<?php include './partials/modals/fitness.php'; ?>

<?php include './partials/_foot.php';
