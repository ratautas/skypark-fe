<div class="app__cookiebar cookiebar" data-cookiebar="./api/cookiebar.php">
  <div class="cookiebar__container">
    <i class="cookiebar__close close" data-cookiebar-accept></i>
    <div class="cookiebar__text">
      <p>By using our website and services you agree to our use of cookies.</p>
      <p>Check our Terms & Privacy <a href="/">here</a>.</p>
    </div>
  </div>
</div>
