<div class="wizard__scene wizard__scene--2 scene _before-tween">
  <svg class="scene__shape scene__shape--background" viewbox="0 0 264 264">
    <circle class="scene__stroke" cx="132" cy="132" r="107" stroke-width="50" stroke="#fdc805" />
  </svg>
  <div class="scene__media scene__media--wizard--2">
    <img class="scene__image" src="./media/selection--2.png" alt="" loading="lazy" />
  </div>
  <h2 class="scene__heading heading" data-wizard-heading><?=$heading; ?></h2>
</div>