<div class="wizard__scene wizard__scene--1 scene">
  <svg class="scene__shape scene__shape--background" viewbox="0 0 264 264">
    <circle class="scene__stroke" cx="132" cy="132" r="107" stroke-width="50" stroke="#fdc805" />
  </svg>
  <div class="scene__media scene__media--wizard--1">
    <img class="scene__image" src="./media/selection--1.png" alt="" loading="lazy" />
  </div>
  <svg class="scene__shape scene__shape--foreground" viewbox="0 0 264 264">
    <circle class="scene__stroke" cx="132" cy="132" stroke-dasharray="224 448"
      stroke-dashoffset="-56" r="107" stroke-width="50" stroke="#fdc805" />
  </svg>
  <h2 class="scene__heading heading" data-wizard-heading><?=$heading; ?></h2>
</div>