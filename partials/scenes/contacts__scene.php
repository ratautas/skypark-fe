<div class="contacts__scene scene _before-tween" data-tweener="scene">
  <div class="scene__media scene__media--contacts" data-tweener-image>
    <img class="scene__image" src="./media/tile__media--lessons.png" alt="" loading="lazy" />
  </div>
  <a href="mailto:<?=$heading; ?>"
    class="scene__heading scene__heading--huge heading" data-tweener-heading>
    <?=$heading; ?>
  </a>
</div>