<div class="gateway__scene gateway__scene--2 scene _before-tween" data-tweener="scene" data-tweener-suspend="300">
  <svg class="scene__shape scene__shape--background" data-tweener-shape viewbox="0 0 226 226">
    <circle class="scene__stroke" cx="113" cy="113" r="88" stroke-width="50" stroke="#d5ad1a" />
  </svg>
  <div class="scene__media scene__media--gateway--2" data-tweener-image>
    <img class="scene__image" src="./media/tile__media--fitness.png" alt="" loading="lazy" />
  </div>
  <svg class="scene__shape scene__shape--foreground" data-tweener-shape viewbox="0 0 226 226">
    <circle class="scene__stroke" cx="113" cy="113" stroke-dasharray="184 184"
      stroke-dashoffset="-184" r="88" stroke-width="50" stroke="#d5ad1a" />
  </svg>
  <div class="scene__heading heading" data-tweener-heading>
    <?=$heading; ?>
  </div>
</div>