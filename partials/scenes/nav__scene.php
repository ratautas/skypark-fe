<div class="nav__scene scene _before-tween" data-tweener="scene" data-tweener="nav">
  <svg class="scene__shape scene__shape--background" viewbox="0 0 345 345" data-tweener-shape>
    <circle cx="172.5" cy="172.5" fill="transparent" r="147.5" stroke-width="50" stroke="#fdc805" />
  </svg>
  <div class="scene__media scene__media--nav" data-tweener-image>
    <img class="scene__image" src="./media/intro__image--privacy.png" alt="" loading="lazy" />
  </div>
  <svg class="scene__shape scene__shape--foreground" viewbox="0 0 345 345" data-tweener-shape>
    <circle cx="172.5" cy="172.5" fill="transparent" stroke-dasharray="309 618"
      stroke-dashoffset="-232" r="147.5" stroke-width="50" stroke="#fdc805" />
  </svg>
</div>