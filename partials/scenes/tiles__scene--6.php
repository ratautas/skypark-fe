<div class="tiles__scene tiles__scene--6 scene _before-tween" data-tweener="scene">
  <div class="scene__media scene__media--tiles--6" data-tweener-image>
    <img class="scene__image" src="./media/tile__media--fitness.png" alt="" loading="lazy" />
  </div>
  <h2 class="scene__heading scene__heading--teal heading" data-tweener-heading>
    <?=$heading; ?>
  </h2>
</div>