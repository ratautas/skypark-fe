<div class="default__scene default__scene--4 scene _before-tween" data-tweener="scene">
  <svg class="scene__shape scene__shape--background" data-tweener-shape viewbox="0 0 120 120">
    <rect class="scene__stroke" width="120" height="120" rx="6" transform="rotate(45,60,60)"
      stroke-width="50" stroke="#fdc805" />
  </svg>
  <div class="scene__media scene__media--default--4" data-tweener-image>
    <img class="scene__image" src="./media/intro__image--booking.png" alt="" loading="lazy" />
  </div>
  <svg class="scene__shape scene__shape--foreground" data-tweener-shape viewbox="0 0 120 120">
    <rect class="scene__stroke" width="120" height="120" rx="6" stroke-dasharray="320"
      stroke-dashoffset="-80" transform="rotate(45,60,60)" stroke-width="50" stroke="#fdc805" />
  </svg>
  <h1 class="scene__heading heading" data-tweener-heading>
    <?=$heading; ?>
  </h1>
</div>