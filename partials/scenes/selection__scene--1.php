<div class="selection__scene selection__scene--1 scene _before-tween" data-tweener="scene">
  <svg class="scene__shape scene__shape--background" data-tweener-shape viewbox="0 0 264 264">
    <circle class="scene__stroke" cx="132" cy="132" r="107" stroke-width="50" stroke="#fdc805" />
  </svg>
  <div class="scene__media scene__media--selection--1" data-tweener-image>
    <img class="scene__image" src="./media/selection--1.png" alt="" loading="lazy" />
  </div>
  <svg class="scene__shape scene__shape--foreground" data-tweener-shape viewbox="0 0 264 264">
    <circle class="scene__stroke" cx="132" cy="132" stroke-dasharray="224 448"
      stroke-dashoffset="-56" r="107" stroke-width="50" stroke="#fdc805" />
  </svg>
  <h2 class="scene__heading heading" data-tweener-heading>
    <?=$heading; ?>
  </h2>
</div>