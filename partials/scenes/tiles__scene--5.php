<div class="tiles__scene tiles__scene--5 scene _before-tween" data-tweener="scene">
  <div class="scene__media scene__media--tiles--5" data-tweener-image>
    <img class="scene__image" src="./media/tile__media--lessons.png" alt="" loading="lazy" />
  </div>
  <h2 class="scene__heading scene__heading--teal heading" data-tweener-heading>
    <?=$heading; ?>
  </h2>
</div>