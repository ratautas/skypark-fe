<div class="hero__scene scene _before-tween" data-tweener="scene">
<!--
  <div class="scene__media scene__media--hero" data-tweener-image>
    <img class="scene__image" src="./media/intro__image--safety.png" alt="" loading="lazy" />
  </div>
  -->
  <h1 class="scene__heading scene__heading--huge heading" data-tweener-heading>
    <?=$heading; ?>
  </h1>
</div>