<div class="default__scene default__scene--1 scene _before-tween" data-tweener="scene">
  <svg class="scene__shape scene__shape--background" data-tweener-shape viewbox="0 0 154 154">
    <circle class="scene__stroke" cx="77" cy="77" r="61" stroke-width="32" stroke="#fdc805" />
  </svg>
  <div class="scene__media scene__media--default--1" data-tweener-image>
    <img class="scene__image" src="./media/intro__image--privacy.png" alt="" loading="lazy" />
  </div>
  <svg class="scene__shape scene__shape--foreground" data-tweener-shape viewbox="0 0 154 154">
    <circle class="scene__stroke" cx="77" cy="77" stroke-dasharray="64 314" stroke-dashoffset="-128"
      r="61" stroke-width="32" stroke="#fdc805" />
  </svg>
  <h1 class="scene__heading heading" data-tweener-heading>
    <?=$heading; ?>
  </h1>
</div>