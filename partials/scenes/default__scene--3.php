<div class="default__scene default__scene--3 scene _before-tween" data-tweener="scene">
  <svg class="scene__shape scene__shape--background" data-tweener-shape viewbox="0 0 152 152">
    <circle class="scene__stroke" cx="76" cy="76" r="56" stroke-width="40" stroke="#fdc805" />
  </svg>
  <div class="scene__media scene__media--default--3" data-tweener-image>
    <img class="scene__image" src="./media/intro__image--safety.png" alt="" loading="lazy" />
  </div>
  <h1 class="scene__heading heading" data-tweener-heading>
    <?=$heading; ?>
  </h1>
</div>