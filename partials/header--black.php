  <div class="header__content header__content--black">
    <a class="header__logo header__logo--black" href="index">
      <?php include './assets/svg/logo.svg'; ?>
    </a>
    <div class="header__triggers">
      <span class="header__trigger header__trigger--hours underlink hours">WORKING HOURS
        <span class="hours__wrapper">
          <span class="hours__row">I-V 12:00 - 22:00</span>
          <span class="hours__row">VI-VII 10:00 - 22:00</span>
        </span>
      </span>
      <a class="header__trigger header__trigger--book underlink" href="booking">BOOK A ROOM</a>
    </div>
  </div>
  <div class="header__navicon navicon navicon--black" data-navicon="open">
    <div class="navicon__label">MENU</div>
    <div class="navicon__icon">
      <i class="navicon__bar navicon__bar--1"></i>
      <i class="navicon__bar navicon__bar--2"></i>
      <i class="navicon__bar navicon__bar--3"></i>
    </div>
  </div>