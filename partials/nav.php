<nav class="app__nav nav" data-nav>
  <div class="nav__container container container--l">
    <div class="nav__visual">
      <?php include './partials/scenes/nav__scene.php'; ?>
    </div>
    <div class="nav__menu">
      <ul class="nav__items">
        <li class="nav__item">
          <a class="nav__trigger" data-nav-heading href="about">Trampoline park</a>
        </li>
        <li class="nav__item">
          <a class="nav__trigger" data-nav-heading href="selection">ROOMS FOR RENT</a>
        </li>
        <li class="nav__item">
          <a class="nav__trigger" data-nav-heading href="pricing">PRICE LIST</a>
        </li>
        <li class="nav__item">
          <a class="nav__trigger" data-nav-heading href="trainings">TRAININGS</a>
        </li>
        <li class="nav__item">
          <a class="nav__trigger" data-nav-heading href="craft-pizza">CRAFT PIZZA</a>
        </li>
        <li class="nav__item">
          <a class="nav__trigger" data-nav-heading href="faq">F.A.Q.</a>
        </li>
        <li class="nav__item">
          <a class="nav__trigger" data-nav-heading href="contacts">CONTACTS</a>
        </li>
      </ul>
      <div class="nav__socials">
        <a class="nav__social nav__social--facebook" data-nav-social href="" target="_blank">
          <?php include './assets/svg/icon--facebook.svg'; ?>
        </a>
        <a class="nav__social nav__social--youtube" data-nav-social href="" target="_blank">
          <?php include './assets/svg/icon--youtube.svg'; ?>
        </a>
      </div>
    </div>
  </div>
  <div class="nav__close close underlink" data-navicon="close">CLOSE</div>
</nav>