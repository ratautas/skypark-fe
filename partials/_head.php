<!doctype html>
<html lang="en">


<head>
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta name="msapplication-TileColor" content="#ddd">
  <meta name="msapplication-TileImage" content="./assets/img/webapp/mstile-144x144.png">
  <meta name="msapplication-config" content="./assets/img/webapp/browserconfig.xml">
  <meta charset="UTF-8">
  <meta content="#f9f6ee" name="theme-color">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
  <meta content="yes" name="apple-mobile-web-app-capable">
  <meta content="Skypark" name="author">
  <meta content="Skypark" name="generator">
  <meta content="noindex, nofollow" name="robots">
  <!-- <meta name="robots" content="index, follow"> -->
  <!-- <meta name="title" content="{{ meta_title }}"> -->
  <!-- <meta name="description" content="{{ meta_description }}"> -->
  <!-- <meta property="og:title" content="{{ meta_title }}" /> -->
  <!-- <meta property="og:description" content="{{ meta_description }}" /> -->
  <!-- <meta property="og:url" content="{{ url_current() }}" /> -->
  <!-- <meta property="og:image" content="{{ meta_image }}" /> -->
  <link rel="shortcut icon" href="./assets/img/webapp/favicon.ico">
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/img/webapp/favicon-16x16.png">
  <link rel="icon" type="image/png" sizes="32x32" href="./assets/img/webapp/favicon-32x32.png">
  <link rel="manifest" href="./assets/img/webapp/manifest.json">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="theme-color" content="#333">
  <meta name="application-name" content="frontier">
  <link rel="apple-touch-icon" sizes="57x57" href="./assets/img/webapp/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="./assets/img/webapp/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="./assets/img/webapp/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/webapp/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114"
    href="./assets/img/webapp/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120"
    href="./assets/img/webapp/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144"
    href="./assets/img/webapp/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152"
    href="./assets/img/webapp/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="167x167"
    href="./assets/img/webapp/apple-touch-icon-167x167.png">
  <link rel="apple-touch-icon" sizes="180x180"
    href="./assets/img/webapp/apple-touch-icon-180x180.png">
  <link rel="apple-touch-icon" sizes="1024x1024"
    href="./assets/img/webapp/apple-touch-icon-1024x1024.png">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta name="apple-mobile-web-app-title" content="frontier">
  <link rel="apple-touch-startup-image"
    media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 1)"
    href="./assets/img/webapp/apple-touch-startup-image-320x460.png">
  <link rel="apple-touch-startup-image"
    media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)"
    href="./assets/img/webapp/apple-touch-startup-image-640x920.png">
  <link rel="apple-touch-startup-image"
    media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
    href="./assets/img/webapp/apple-touch-startup-image-640x1096.png">
  <link rel="apple-touch-startup-image"
    media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"
    href="./assets/img/webapp/apple-touch-startup-image-750x1294.png">
  <link rel="apple-touch-startup-image"
    media="(device-width: 414px) and (device-height: 736px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 3)"
    href="./assets/img/webapp/apple-touch-startup-image-1182x2208.png">
  <link rel="apple-touch-startup-image"
    media="(device-width: 414px) and (device-height: 736px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 3)"
    href="./assets/img/webapp/apple-touch-startup-image-1242x2148.png">
  <link rel="apple-touch-startup-image"
    media="(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 1)"
    href="./assets/img/webapp/apple-touch-startup-image-748x1024.png">
  <link rel="apple-touch-startup-image"
    media="(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 1)"
    href="./assets/img/webapp/apple-touch-startup-image-768x1004.png">
  <link rel="apple-touch-startup-image"
    media="(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2)"
    href="./assets/img/webapp/apple-touch-startup-image-1496x2048.png">
  <link rel="apple-touch-startup-image"
    media="(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2)"
    href="./assets/img/webapp/apple-touch-startup-image-1536x2008.png">
  <link rel="icon" type="image/png" sizes="228x228" href="./assets/img/webapp/coast-228x228.png">
  <link rel="yandex-tableau-widget" href="./assets/img/webapp/yandex-browser-manifest.json">
  <link href="./assets/css/app.css" rel="stylesheet">
  <title>App Title</title>
  <link rel="stylesheet" href="./assets/css/app.css">
</head>

<body class="app">
  <?php include './partials/nav.php';
