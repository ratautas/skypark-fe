<style type="text/css">
#mmm {
  position: fixed;
  width: 260px;
  right: -260px;
  top: 0;
  background: #0c0c0c;
  z-index: 200;
  padding: 15px 15px 15px 30px;
  top: 0;
  height: 100%;
  box-sizing: border-box;
  z-index: 999;
  cursor: pointer;
}

#mmm:hover {
  right: 0;
}

#iii {
  overflow-y: scroll;
  position: absolute;
  width: 100%;
  top: 30px;
  height: 85%;
}

#mmm a {
  text-decoration: none;
  color: #fff;
  font-size: 14px;
  display: block;
  color: #666;
  margin: 5px 0;
  transition: color 100ms ease-in-out;
}

#mmm a:hover {
  color: #fff;
}

#ooo {
  width: 30px;
  height: 29px;
  position: absolute;
  bottom: 0;
  left: -30px;
  background: #0c0c0c;
  border-bottom-left-radius: 2px;
}

#ooo:before {
  content: "";
  position: absolute;
  right: 7px;
  display: block;
  width: 16px;
  top: -1px;
  height: 0;
  box-shadow: 0 10px 0 1px #fff, 0 16px 0 1px #fff, 0 22px 0 1px #fff;
}
</style>
<?php
echo '<div id="mmm"><div id="ooo"></div><div id="iii">';
$files=scandir(getcwd());
$host = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
$url = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/') + 1);
foreach($files as $k => $file)
    if (strpos($file,'.php') !== false && $file !== 'webhook.php') {
        echo '<a href="'.str_replace('.php', '', $host.$url.$file).'">'.str_replace('.php', '', $file).'</a>';
    }
echo '</div></div>';
?>
