<div class="app__modal modal" data-modal="wizard">
  <div class="modal__dialog">
    <div class="modal__wizard wizard" data-wizard>
      <i class="modal__close-icon" data-modal-close></i>
      <form action="success" class="wizard__form" novalidate="novalidate" name="wizard"
        data-wizard-form>
        <div class="wizard__header">
          <div class="wizard__visual" data-wizard-scene="room1">
            <?php $heading = 'PASAKA'; include './partials/scenes/wizard__scene--1.php'; ?>
          </div>
          <div class="wizard__visual" data-wizard-scene="room2">
            <?php $heading = 'JŪRA'; include './partials/scenes/wizard__scene--2.php'; ?>
          </div>
          <div class="wizard__visual" data-wizard-scene="room3">
            <?php $heading = 'PASAKA'; include './partials/scenes/wizard__scene--1.php'; ?>
          </div>
          <div class="wizard__visual" data-wizard-scene="room4">
            <?php $heading = 'JŪRA'; include './partials/scenes/wizard__scene--2.php'; ?>
          </div>
          <div class="wizard__visual wizard__visual--step" data-wizard-visual="1">
            <img alt="" class="wizard__image" loading="lazy" src="./media/catering__image.png" />
          </div>
        </div>
        <div class="wizard__content">
          <div class="wizard__scroller" data-wizard-scroller>
            <div class="wizard__bar">
              <div class="wizard__triggers wizard__triggers--clip" data-wizard-bar>
                <div class="wizard__trigger">INFO</div>
                <div class="wizard__trigger">CATERING</div>
                <div class="wizard__trigger">ADDITIONAL SERVICES</div>
                <div class="wizard__trigger">CONFIRMATION</div>
                <div class="wizard__trigger">PAYMENT</div>
              </div>
              <div class="wizard__triggers">
                <div class="wizard__trigger" data-wizard-trigger="0">INFO</div>
                <div class="wizard__trigger" data-wizard-trigger="1">CATERING</div>
                <div class="wizard__trigger" data-wizard-trigger="2">ADDITIONAL SERVICES</div>
                <div class="wizard__trigger" data-wizard-trigger="3">CONFIRMATION</div>
                <div class="wizard__trigger" data-wizard-trigger="4">PAYMENT</div>
              </div>
              <div class="wizard__timer timer">
                <div class="timer__label">Time left</div>
                <div class="timer__time">
                  <span data-wizard-minutes>15</span>:<span data-wizard-seconds>00</span>
                </div>
              </div>
            </div>
          </div>
          <div class="wizard__steps" data-wizard-stepper>
            <div class="wizard__step wizard__step--info is-active" data-wizard-step="0">
              <div class="wizard__info info">
                <div class="info__columns">
                  <div class="info__column info__column--1">
                    <div class="info__subtitle">child information</div>
                    <div class="info__rows">
                      <div class="info__row">
                        <label class="info__field field">
                          <input class="field__input" name="wizard-child-name" required
                            minlength="3" type="text" />
                          <span class="field__placeholder">Name</span>
                          <span class="field__error">Error</span>
                        </label>
                      </div>
                      <div class="info__row">
                        <label class="info__field field">
                          <input class="field__input" name="wizard-child-age" required
                            type="number" />
                          <span class="field__placeholder">Age</span>
                          <span class="field__error">Error</span>
                        </label>
                      </div>
                      <div class="info__row info__row--radios">
                        <span class="info__gender-label">Gender:</span>
                        <label class="info__radio field field--control">
                          <input class="field__native" name="wizard-gender" checked value="boy"
                            type="radio" />
                          <span class="field__control field__control--radio"></span>
                          <span class="field__inlabel field__inlabel--dark">Boy</span>
                        </label>
                        <label class="info__radio field field--control">
                          <input class="field__native" name="wizard-gender" value="girl"
                            type="radio" />
                          <span class="field__control field__control--radio"></span>
                          <span class="field__inlabel field__inlabel--dark">Girl</span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="info__column info__column--2">
                    <div class="info__subtitle">RESPONSIBLE PARENT information</div>
                    <div class="info__rows">
                      <div class="info__row">
                        <label class="info__field field">
                          <input class="field__input" name="wizard-name" required minlength="3"
                            type="text" />
                          <span class="field__placeholder">Name</span>
                          <span class="field__error">Error</span>
                        </label>
                      </div>
                      <div class="info__row">
                        <label class="info__field field">
                          <input class="field__input" name="wizard-tel" type="tel"
                            inputmode="decimal" required minlength="8" />
                          <span class="field__placeholder">Phone number</span>
                          <span class="field__error">Error</span>
                        </label>
                      </div>
                      <div class="info__row">
                        <label class="info__field field">
                          <input class="field__input" name="wizard-email" required type="email" />
                          <span class="field__placeholder">E-mail address</span>
                          <span class="field__error">Error</span>
                        </label>
                      </div>
                      <div class="info__row info__row--radios">
                        <label class="info__radio field field--control">
                          <input class="field__native" name="wizard-payment-time" checked
                            value="advance" type="radio" />
                          <span class="field__control field__control--radio"></span>
                          <span class="field__inlabel field__inlabel--dark">Pay in advance</span>
                        </label>
                        <label class="info__radio field field--control">
                          <input class="field__native" name="wizard-payment-time" value="total"
                            type="radio" />
                          <span class="field__control field__control--radio"></span>
                          <span class="field__inlabel field__inlabel--dark">Pay total amount</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="info__footer">
                  <label class="info__notes field field--textarea">
                    <span class="field__label">Notes</span>
                    <textarea name="wizard-notes" rows="4" class="field__textarea"></textarea>
                  </label>
                </div>
              </div>
            </div>
            <div class="wizard__step wizard__step--catering" data-wizard-step="1">
              <div class="wizard__catering catering">
                <div class="catering__content">
                  <div class="catering__intro">Duis aute irure dolor in reprehenderit elit seddo
                  </div>
                  <div class="catering__text">
                    <p>Lorem ipsum dolor sit amet, consectetur lorem adipiscing elit sed do eiusmod
                      tempor incidid ut labore et dolore magna aliqua. Ut enim ad minim veniamquis.
                    </p>
                  </div>
                </div>
                <div class="catering__media catering__media--2">
                  <img alt="" class="catering__image" loading="lazy"
                    src="./media/catering__image--2.png" />
                </div>
                <div class="catering__media catering__media--3">
                  <img alt="" class="catering__image" loading="lazy"
                    src="./media/catering__image--3.png" />
                </div>
              </div>
              <a href="" class="wizard__download" download target="_blank">
                <?php include './assets/svg/download.svg'; ?>
                Download our menu
              </a>
            </div>
            <div class="wizard__step wizard__step--services" data-wizard-step="2">
              <div class="wizard__services services">
                <div class="services__intro">We offer some extraordinary opportunities to make the
                  celebration even more memorable at Skypark.</div>
                <div class="services__blocks">
                  <div class="services__block">
                    <div class="services__media">
                      <img src="./media/services__image--1.png" alt="" class="services__image" />
                    </div>
                    <div class="services__label">Lorem ipsum dolor</div>
                  </div>
                  <div class="services__block">
                    <div class="services__media">
                      <img src="./media/services__image--2.png" alt="" class="services__image" />
                    </div>
                    <div class="services__label">Duis nostrud exercit</div>
                  </div>
                  <div class="services__block">
                    <div class="services__media">
                      <img src="./media/services__image--1.png" alt="" class="services__image" />
                    </div>
                    <div class="services__label">Lorem ipsum dolor</div>
                  </div>
                  <div class="services__block">
                    <div class="services__media">
                      <img src="./media/services__image--2.png" alt="" class="services__image" />
                    </div>
                    <div class="services__label">Duis nostrud exercit</div>
                  </div>
                </div>
                <div class="services__text">
                  <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat lorem ipsum sitametdolor.</p>
                </div>
              </div>
            </div>
            <div class="wizard__step wizard__step--confirm" data-wizard-step="3">
              <div class="wizard__confirm confirm">
                <div class="confirm__columns">
                  <div class="confirm__column confirm__column--1">
                    <div class="confirm__subtitle">child</div>
                    <div class="confirm__blocks">
                      <div class="confirm__block">
                        <div class="confirm__label">Name:</div>
                        <div class="confirm__value">Vardas Pavardė</div>
                      </div>
                      <div class="confirm__block">
                        <div class="confirm__label">Age:</div>
                        <div class="confirm__value">14</div>
                      </div>
                      <div class="confirm__block">
                        <div class="confirm__label">Gender:</div>
                        <div class="confirm__value">Boy</div>
                      </div>
                    </div>
                  </div>
                  <div class="confirm__column confirm__column--2">
                    <div class="confirm__subtitle">RESPONSIBLE PARENT</div>
                    <div class="confirm__blocks">
                      <div class="confirm__block">
                        <div class="confirm__label">Name:</div>
                        <div class="confirm__value">Vardas Pavardė</div>
                      </div>
                      <div class="confirm__block">
                        <div class="confirm__label">Phone number:</div>
                        <div class="confirm__value">+370 662 38 774</div>
                      </div>
                    </div>
                  </div>
                  <div class="confirm__column confirm__column--3">
                    <div class="confirm__subtitle">NOTES</div>
                    <div class="confirm__notes">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Ncididunt ut labore et dolore magna aliqua minim veniam, quis
                        nostrud exercitation ullamco.</p>
                    </div>
                  </div>
                </div>
                <label class="confirm__agreement field field--control">
                  <input class="field__native" name="wizard-terms" required type="checkbox" />
                  <i class="field__control field__control--checkbox"></i>
                  <span class="field__inlabel field__inlabel--dark">
                    <p>I have read and accept the
                      <span data-modal-trigger="rules">terms of use</span>
                    </p>
                  </span>
                  <span class="field__error">Error</span>
                </label>
              </div>
            </div>
            <div class="wizard__step wizard__step--payment" data-wizard-step="4">
              <div class="wizard__payment payment">
                <div class="payment__container">
                  <div class="payment__subtitle">COOSE PAYMENT METHOD</div>
                  <div class="payment__types">
                    <label class="payment__type field field--control">
                      <input class="field__native" name="wizard-payment-type" checked
                        value="banklink" type="radio" />
                      <span class="field__control field__control--radio"></span>
                      <span class="field__inlabel field__inlabel--dark">Banklink</span>
                    </label>
                    <label class="payment__type field field--control">
                      <input class="field__native" name="wizard-payment-type" value="transfer"
                        type="radio" />
                      <span class="field__control field__control--radio"></span>
                      <span class="field__inlabel field__inlabel--dark">Bank transfer</span>
                    </label>
                  </div>
                  <div class="payment__info">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitsed eiusmod tempor
                      lorem ipsumsit ametdolor Skypark incididunt ut:</p>
                  </div>
                  <div class="payment__content">
                    <div class="payment__banks is-active" data-wizard-payment-type="banklink">
                      <div class="payment__bank">
                        <div class="payment__block">
                          <input class="payment__radio" checked type="radio" name="wizard-bank"
                            value="swedbank">
                          <img src="./media/swedbank.png" alt="swedbank" class="payment__logo">
                        </div>
                      </div>
                      <div class="payment__bank">
                        <div class="payment__block">
                          <input class="payment__radio" type="radio" name="wizard-bank" value="seb">
                          <img src="./media/seb.png" alt="seb" class="payment__logo">
                        </div>
                      </div>
                      <div class="payment__bank">
                        <div class="payment__block">
                          <input class="payment__radio" type="radio" name="wizard-bank"
                            value="luminor">
                          <img src="./media/luminor.png" alt="luminor" class="payment__logo">
                        </div>
                      </div>
                    </div>
                    <div class="payment__credentials" data-wizard-payment-type="transfer">
                      <p>
                        <b>Receiver name:</b>&nbsp;Skypark</p>
                      <p>
                        <b>Account number:</b>&nbsp;LT13 2140 0300 0121 3368
                      </p>
                      <p>
                        <b>The purpose of payment:</b>&nbsp;name, room title, date of celebration.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="wizard__actions">
            <div class="wizard__total">
              <span class="wizard__total-label">Total:</span>
              <span class="wizard__total-value">
                <span data-wizard-total></span>&nbsp;&euro;
              </span>
            </div>
            <div class="wizard__action wizard__action--back btn" data-wizard-back>
              <span class="btn__text">BACK</span>
            </div>
            <div class="wizard__action wizard__action--next btn btn--yellow" data-wizard-next>
              <span class="btn__text">NEXT</span>
            </div>
            <button class="wizard__action wizard__action--confirm btn btn--yellow">
              <span class="btn__text">CONFIRM</span>
            </button>
          </div>
        </div>
        <input type="hidden" name="wizard-room" />
        <input type="hidden" name="wizard-time" />
        <input type="hidden" name="wizard-price" />
      </form>
    </div>
  </div>
  <i class="modal__backdrop" data-modal-close></i>
</div>

<?php include './partials/modals/rules.php';
