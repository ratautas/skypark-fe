<div class="app__modal modal modal--white" data-modal="fitness">
  <div class="modal__dialog modal__dialog--full">
    <div class="modal__header">
      <div class="modal__close-text close underlink" data-modal-close>CLOSE</div>
    </div>
    <div class="modal__fitness fitness">
      <form action="" name="fitness" class="fitness__form">
        <div class="fitness__columns">
          <div class="fitness__column fitness__column--times">
            <div class="fitness__subtitle">SELECT YOUR TIME</div>
            <div class="fitness__info">In order to book, you need to select your arrival time</div>
            <div class="fitness__times">
              <label class="fitness__select">
                <span class="fitness__block">
                  <input type="radio" name="time" checked value="aerobics-03-10" class="fitness__native field__native">
                  <div class="fitness__label">Aerobics</div>
                  <div class="fitness__weekday">Wednesday</div>
                  <div class="fitness__time">10:00</div>
                </span>
              </label>
              <label class="fitness__select">
                <span class="fitness__block">
                  <input type="radio" name="time" value="fitness-05-16" class="fitness__native field__native">
                  <div class="fitness__label">Fitness</div>
                  <div class="fitness__weekday">Friday</div>
                  <div class="fitness__time">16:00</div>
                </span>
              </label>
              <label class="fitness__select">
                <span class="fitness__block">
                  <input type="radio" name="time" value="aerobics-03-16" class="fitness__native field__native">
                  <div class="fitness__label">Aerobics</div>
                  <div class="fitness__weekday">Wednesday</div>
                  <div class="fitness__time">16:00</div>
                </span>
              </label>
              <label class="fitness__select">
                <span class="fitness__block">
                  <input type="radio" name="time" value="fitness-05-10" class="fitness__native field__native">
                  <div class="fitness__label">Fitness</div>
                  <div class="fitness__weekday">Friday</div>
                  <div class="fitness__time">10:00</div>
                </span>
              </label>
            </div>
          </div>
          <div class="fitness__column fitness__column--details">
            <div class="fitness__subtitle">FILL YOUR INFORMATION</div>
            <div class="fitness__info">To complete your booking, fill your information</div>
            <div class="fitness__details">
              <div class="fitness__row">
                <label class="fitness__field field">
                  <input class="field__input" name="name" required minlength="3" type="text"/>
                  <span class="field__placeholder">Your name</span>
                  <span class="field__error">Error</span>
                </label>
              </div>
              <div class="fitness__row">
                <label class="fitness__field field">
                  <input class="field__input" name="surname" required minlength="3" type="text"/>
                  <span class="field__placeholder">Your surname</span>
                  <span class="field__error">Error</span>
                </label>
              </div>
              <div class="fitness__row">
                <label class="fitness__field field">
                  <input class="field__input" name="child-name" minlength="3" type="text"/>
                  <span class="field__placeholder">Child name
                    <i>(optional)</i>
                  </span>
                  <span class="field__error">Error</span>
                </label>
              </div>
              <div class="fitness__row">
                <label class="fitness__field field">
                  <input class="field__input" name="tel" type="tel" inputmode="decimal" required minlength="8"/>
                  <span class="field__placeholder">Phone number</span>
                  <span class="field__error">Error</span>
                </label>
              </div>
              <div class="fitness__row">
                <label class="fitness__field field">
                  <input class="field__input" name="email" required type="email"/>
                  <span class="field__placeholder">E-Mail</span>
                  <span class="field__error">Error</span>
                </label>
              </div>
              <div class="fitness__row fitness__row--rules">
                <label class="fitness__field field field--control">
                  <input class="field__native" name="rules" required type="checkbox"/>
                  <i class="field__control field__control--checkbox"></i>
                  <span class="field__inlabel">
                    <p>I have read and accept the
                      <a href="/">rules</a>
                    </p>
                  </span>
                  <span class="field__error">Error</span>
                </label>
              </div>
              <div class="fitness__row fitness__row--submit">
                <button class="fitness__submit btn btn--yellow">
                  <span class="btn__text">BOOK</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
