<div class="app__modal modal modal--white" data-modal="map">
  <div class="modal__dialog modal__dialog--full">
    <div class="modal__header">
      <div class="modal__close-text close underlink" data-modal-close>CLOSE</div>
    </div>
    <div class="modal__map map">
      <div class="map__embed" data-map="contacts" data-map-zoom="15.5" data-map-center-lat="56.2031"
        data-map-center-lng="24.76" data-map-markers='[{"lat": 56.2031,"lng": 24.76,"pin": "http://localhost/skypark/assets/img/marker.svg"}]'></div>
      <div class="map__address">Verkių g. 29, Ogmios miestas, Šeimos aikštelė 3 Vilnius</div>
    </div>
  </div>
</div>