<div class="app__modal modal" data-modal="promo" data-promo="unique-promo-slug"
  data-promo-delay="3000">
  <div class="modal__dialog">
    <div class="modal__promo promo">
      <i class="modal__close-icon" data-modal-close></i>
      <div class="promo__media">
        <img alt="" class="promo___image" loading="lazy" src="./media/promo__image.png" title="" />
      </div>
      <div class="promo__content">
        <div class="promo__intro">Duis aute irure dolor in reprehenderit in voluptate</div>
        <div class="promo__text">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip</p>
        </div>
        <div class="promo__actions">
          <a href="" target="_blank" class="promo__action btn btn--yellow">
            <span class="btn__text">READ MORE</span>
          </a>
        </div>
      </div>
    </div>
  </div>
  <i class="modal__backdrop" data-modal-close></i>
</div>