<?php include './partials/_head.php'; ?>



<div class="app__page app__page--invitation invitation" data-invitation>

  <header class="invitation__header header header--white">
    <?php include './partials/header--white.php'; ?>
  </header>

  <div class="invitation__container container">

    <div class="invitation__intro">
      <div class="invitation__heading heading _before-tween" data-tweener="scene">
        <h1 data-tweener-heading>Vardenio pavardenio<br>
          Gimtadienio šventė</h1>
      </div>
      <div class="invitation__text">
        <p>adipisicing elit sed doLorem Ieiusmod tempor incididunt ut labore etdolore magna aliqua
          lorem ipsumsit ametdolor doLorem</p>
      </div>
    </div>

    <div class="invitation__blocks">

      <div class="invitation__block invitation__block--1">
        <div class="invitation__label">Birželio 20 d.</div>
        <div class="invitation__value">16:00 val.</div>
      </div>

      <div class="invitation__block invitation__block--2">
        <div class="invitation__label">Žaidimų kambarys</div>
        <div class="invitation__value">„pasaka“</div>
      </div>

    </div>

    <div class="invitation__address" data-modal-trigger="map">
      <?php include './assets/svg/icon--pin.svg'; ?>
      Verkių g. 29, Ogmios miestas, Šeimos aikštelė 3 Vilnius
    </div>
  </div>

  <canvas class="invitation__confetti" data-confetti="1500"></canvas>

</div>

<?php include './partials/modals/map.php'; ?>

<?php include './partials/_foot.php';
