<?php include './partials/_head.php'; ?>

<div class="app__page app__page--home home">

  <section class="home__hero hero" style="background-image:url('./media/hero__media.png')">

    <header class="hero__header header header--white">
      <?php include './partials/header--white.php'; ?>
    </header>


    <?php $heading = 'HAVING NO WINGS<br>IS JUST A LITTLE DETAIL'; include './partials/scenes/hero__scene.php'; ?>

    <div class="hero__play play _before-tween" data-modal-trigger="video" data-tweener="scene"
      data-tweener-suspend="700">
      <i class="play__icon" data-tweener-shape><?php include './assets/svg/play.svg'; ?></i>
      <span class="play__label" data-tweener-heading>PLAY VIDEO</span>
    </div>

    <div class="hero__scrollto scrollto" data-scrollto-trigger=".tiles">
      <div class="scrollto__label">I’M READY TO JUMP</div>
      <i class="scrollto__icon"><?php include './assets/svg/scrollto__icon.svg'; ?></i>
    </div>

  </section>

  <section class="home__tiles tiles">

    <header class="tiles__header header header--white">
      <?php include './partials/header--black.php'; ?>
    </header>

    <i class="tiles__frame"></i>

    <div class="tiles__blocks">

      <div class="tiles__block">

        <div class="tiles__content">
          <?php $heading = 'ARRIVING FOR THE<br>FIRST TIME'; include './partials/scenes/tiles__scene--1.php'; ?>
        </div>

        <a class="tiles__trigger" href=""></a>
      </div>

      <div class="tiles__block">

        <div class="tiles__content">
          <?php $heading = 'SAFETY'; include './partials/scenes/tiles__scene--2.php'; ?>
        </div>
        <a class="tiles__trigger" href=""></a>

      </div>

      <div class="tiles__block">

        <div class="tiles__content">
          <?php $heading = 'ROOMS'; include './partials/scenes/tiles__scene--3.php'; ?>
        </div>
        <a class="tiles__trigger" href=""></a>
      </div>

      <div class="tiles__block">

        <div class="tiles__content">
          <?php $heading = 'CRAFT PIZZA'; include './partials/scenes/tiles__scene--4.php'; ?>
        </div>
        <a class="tiles__trigger" href=""></a>
      </div>

      <div class="tiles__block">

        <div class="tiles__content">
          <?php $heading = 'PRIVATE<br>LESSONS'; include './partials/scenes/tiles__scene--5.php'; ?>
        </div>
        <a class="tiles__trigger" href=""></a>
      </div>

      <div class="tiles__block">

        <div class="tiles__content">
          <?php $heading = 'FITNESS<br>TRAININGS'; include './partials/scenes/tiles__scene--6.php'; ?>
        </div>
        <a class="tiles__trigger" href=""></a>
      </div>

    </div>

  </section>

</div>

<?php include './partials/modals/video.php'; ?>

<?php include './partials/_foot.php';
