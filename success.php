<?php include './partials/_head.php'; ?>



<div class="app__page app__page--success success">

  <header class="success__header header header--black">
    <?php include './partials/header--white.php'; ?>
  </header>

  <div class="success__container container">

    <div class="success__heading heading _before-tween" data-tweener="scene">
      <h1 data-tweener-heading>You have successfully<br>
        bookED a room</h1>
    </div>
    <a href="arriving" class="success__cta btn btn--white _before-tween" data-tweener="cta"
      data-tweener-suspend="1800">
      <span class="btn__text">WHAT TO KNOW BEFORE YOU COME</span>
    </a>

  </div>

  <canvas class="success__confetti" data-confetti="0"></canvas>

</div>

<?php include './partials/modals/map.php'; ?>

<?php include './partials/_foot.php';
