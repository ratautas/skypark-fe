const captureClicks = (callback) => {
  document.addEventListener('click', callback);
};

const clickHandler = (event) => {
  console.log(event);
};

// A sweet util for overriding the callback behavior
// const updateHandler = (originalCallback) => {
//   const newCallback = (event) => {
//     originalCallback(event.x);
//   };
//   return newCallback;
// };

const updateHandler = (originalHandler) => (event) => originalHandler(event.x);

captureClicks(updateHandler(clickHandler)); // now you gotta call `setup` for anything to run
