import canvasConfetti from 'canvas-confetti';

import $$ from '../toolkit/$$';

export default class Confetti {
  constructor() {
    if ($$('[data-confetti]')[0]) {
      const delay = Number($$('[data-confetti]')[0].dataset.confetti);
      setTimeout(() => {
        canvasConfetti.create($$('[data-confetti]')[0], { resize: true })({
          particleCount: 1500,
          angle: 90,
          startVelocity: 120,
          ticks: 1000,
          spread: 90,
          origin: {
            x: 0.5,
            y: 1.5,
          },
        });
      }, delay);
    }
  }
}
