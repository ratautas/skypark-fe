import animejs from 'animejs';

import Heading from '../components/Heading';

import $$ from '../toolkit/$$';

export default class Nav {
  $nav: HTMLElement = $$('[data-nav]')[0];
  $$openIcons: NodeListOf<HTMLElement> = $$('[data-navicon="open"]');
  $$closeIcons: NodeListOf<HTMLElement> = $$('[data-navicon="close"]');

  $$headings: NodeListOf<HTMLElement> = $$('[data-nav-heading]');
  $$socials: NodeListOf<HTMLElement> = $$('[data-nav-social]');

  isOpen: boolean = document.body.matches('.nav-open');

  openTimeline: any;
  closeTimeline: any;

  constructor() {
    this.setupAnimations();
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);

    this.$$openIcons.forEach($icon => ($icon.onclick = this.open));
    this.$$closeIcons.forEach($icon => ($icon.onclick = this.close));

    document.addEventListener('keydown', (e: any) => {
      if (e.key === 'Escape') this.close();
    });
  }

  setupAnimations() {
    let delay = 0;
    this.openTimeline = animejs.timeline({
      autoplay: false,
      targets: this.$nav,
      easing: 'linear',
      opacity: [0, 1],
      duration: 150,
      complete: () => (this.isOpen = true),
    });

    [].forEach.call(this.$$headings, ($heading, h) => {
      new Heading($heading).symbolAnimations.forEach((animation, a) => {
        delay = h * 120 + a * 18;
        animation.delay = delay;
        animation.opacity = [0, 1];
        this.openTimeline.add({ ...animation }, 0);
      });
    });

    this.openTimeline.add(
      {
        targets: this.$$socials,
        easing: 'spring(1, 80, 10, 0)',
        translateY: [300, 0],
        delay: (el, i) => i * 50,
      },
      delay,
    );

    this.closeTimeline = animejs.timeline({
      autoplay: false,
      easing: 'linear',
      targets: this.$nav,
      opacity: 0,
      duration: 500,
      complete: () => {
        this.isOpen = false;
        document.body.classList.remove('nav-open');
        animejs.set(this.$nav, { display: 'none' });
      },
    });
  }

  public open() {
    animejs.set(this.$nav, { display: 'block' });
    document.body.classList.add('nav-open');
    this.$$openIcons.forEach($icon => $icon.classList.add('is-open'));
    this.openTimeline.play();
  }

  public close() {
    this.$$openIcons.forEach($icon => $icon.classList.remove('is-open'));
    this.closeTimeline.play();
  }
}
