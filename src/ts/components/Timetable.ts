import { IVLDField } from '../modules/vld/VLD.d';

import $$ from '../toolkit/$$';
import VLDFieldSelect from '../modules/vld/VLD.Field.Select';

export default class Timetable {
  // Timetable container element
  $timetable: HTMLElement;
  // Timetable select element
  $select: HTMLSelectElement;
  // Initialized vld field
  choices: any;

  constructor() {
    if ($$('[data-timetable]')[0]) this.init();
  }

  init() {
    this.$timetable = $$('[data-timetable]')[0];
    this.$select = $$('[data-timetable-choices]')[0];
    this.$select.addEventListener('change', () => (location.href = this.$select.value));
    this.choices = new VLDFieldSelect(this.$select);
  }
}
