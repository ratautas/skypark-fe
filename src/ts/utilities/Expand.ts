import { slideDown, slideUp, slideStop } from 'slide-anim';

export default class Expand {
  $expand: HTMLElement;
  $expandItems: NodeListOf<HTMLElement>;
  $activeItem: HTMLElement | null = null;

  constructor($expand) {
    this.$expand = $expand;
    this.$expandItems = this.$expand.querySelectorAll('[data-expand-item]');
    this.$activeItem = this.$expand.querySelector('[data-expand-item].is-active');

    this.$expandItems.forEach(($item: HTMLElement) => {
      $item.querySelector('[data-expand-trigger]').addEventListener('click', () => {
        if ($item.matches('.is-active')) {
          this.closeExpand($item);
        } else {
          if (this.$activeItem) {
            this.closeExpand(this.$activeItem);
          }
          this.openExpand($item);
        }
      });
    });

    this.checkOnInit();
  }

  openExpand($item) {
    const $content = $item.querySelector('[data-expand-content]');
    slideStop($content);
    slideDown($content).then(() => {
      $item.classList.add('is-active');
      this.$activeItem = $item;
      window.location.hash = $item.dataset.expandItem;
    });
  }

  closeExpand($item) {
    const $content = $item.querySelector('[data-expand-content]');
    slideStop($content);
    slideUp($content).then(() => {
      $item.classList.remove('is-active');
      this.$activeItem = null;
    });
  }

  checkOnInit() {
    const id = window.location.hash.replace('#', '');
    if (document.querySelector(`[data-expand-item="${id}"]`)) {
      this.openExpand(document.querySelector(`[data-expand-item="${id}"]`));
    }
  }
}
