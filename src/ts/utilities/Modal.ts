import { enter, leave } from '../toolkit/transition';
import removeLocationHash from '../toolkit/removeLocationHash';
import getTransitionDuration from '../toolkit/getTransitionDuration';

interface HTMLPlayerElement extends HTMLElement {
  play: () => void;
  pause: () => void;
}

export default class Modal {
  $modal: HTMLElement;
  $$closes: NodeListOf<HTMLElement>;
  $$triggers: NodeListOf<HTMLElement>;
  $player: HTMLPlayerElement;
  id: string;
  transition: number;
  isOpen: boolean;
  constructor($modal) {
    this.$modal = $modal;
    this.id = $modal.dataset.modal;
    this.$player = $modal.querySelector('[data-player]');
    this.$$closes = $modal.querySelectorAll('[data-modal-close]');
    this.$$triggers = document.querySelectorAll(`[data-modal-trigger="${this.id}"]`);
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.transition = getTransitionDuration($modal);
    this.init();
  }

  init() {
    [].forEach.call(this.$$triggers, $trigger => $trigger.addEventListener('click', this.open));
    [].forEach.call(this.$$closes, $close => $close.addEventListener('click', this.close));
    document.addEventListener('keydown', (e: any) => {
      if (e.key === 'Escape' && this.isOpen) this.close();
    });
    if (location.hash.length && location.hash === `#${this.id}`) this.open();
  }

  open() {
    enter(this.$modal);
    location.hash = `#${this.id}`;
    if (this.$player) setTimeout(() => this.$player.play(), this.transition);
    this.isOpen = true;
    document.body.classList.add('modal-open');
    document.dispatchEvent(new CustomEvent('body:fix'));
  }

  close() {
    if (this.$player) this.$player.pause();
    removeLocationHash();
    this.isOpen = false;
    document.body.classList.remove('modal-open');
    leave(this.$modal);
    document.dispatchEvent(new CustomEvent('body:release'));
  }
}
