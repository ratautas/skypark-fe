import FormField from './FormField';

/**
 * Checkbox field creation class
 * @class CheckBoxField
 * @extends {FormField}
 */

class CheckBoxField extends FormField {
  /**
   * Creates an instance of CheckBoxField.
   * @param {HTMLInputElement} $field
   * @memberof CheckBoxField
   */

  constructor($field: HTMLInputElement) {
    super($field);
  }

  /**
   * Check if field has value (is checked)
   * @returns Boolean value, true for false
   */
  public hasValue() {
    return Boolean(this.$field.checked);
  }

  /**
   * Update field object's value and start validation.
   */
  public handleChange() {
    // Checkboxes are checked not for value, but against 'checked' property.
    this.$field.value = this.$field.checked ? 'on' : '';
    this.updateState({
      hasValue: this.hasValue(),
      isValid: this.applyValidators(),
      hasError: !this.applyValidators(),
    });
  }
}

export default CheckBoxField;
