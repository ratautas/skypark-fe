import FormField from '../form/FormField';
import EmailField from '../form/EmailField';
import CheckBoxField from '../form/CheckBoxField';
import RadioField from '../form/RadioField';
import SelectField from '../form/SelectField';

/**
 * Form Factory (initializer class) - abstracts Field creatiion and attaches Observers.
 */

class FieldFactory {
  /**
   *Creates an instance of FieldFactory.
   * @param {IField} $field
   * @memberof FieldFactory
   */

  constructor($field: IField) {
    if ($field.type === 'email') return new EmailField($field);
    if ($field.type === 'checkbox') return new CheckBoxField($field);
    if ($field.type === 'radio') return new RadioField($field);
    if ($field.type === 'select-one') return new SelectField($field);
    return new FormField($field); // default return
  }
}

export default FieldFactory;
