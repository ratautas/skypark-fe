import FormField from './FormField';

/**
 * Email field creation class
 * @class EmailField
 * @extends {FormField}
 */

class EmailField extends FormField {
  /**
   * Creates an instance of EmailField.
   * @param {HTMLInputElement} $field
   * @memberof EmailField
   */
  constructor($field: HTMLInputElement) {
    super($field);
  }

  /**
   * Add additional field-specific validation. In this case email regex validation
   * @returns Same constructor with updated rules
   */
  public onMount() {
    // Add validation for fields with regex patterns
    // tslint:disable-next-line:max-line-length
    const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.addValidationRule(() => emailRegExp.test(this.$field.value));
    return this;
  }
}

export default EmailField;
