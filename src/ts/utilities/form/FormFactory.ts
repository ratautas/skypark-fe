import FieldFactory from '../form/FieldFactory';

/**
 * Form Factory (initializer class) - abstracts all form creation and listens to field events.
 */
class FormFactory {
  /**
   * Creates an instance of FormFactory.
   * @param {NodeListOf<HTMLFormElement>} $forms - A nodelist of <form> elements.
   */
  constructor($forms: NodeListOf<HTMLFormElement>) {
    [].forEach.call($forms, $form => this.mount($form));
  }

  /**
   * Initialize a form - attach submission events and mount fields.
   * @private
   * @param {HTMLFormElement} $form -
   * @returns
   */
  private mount($form: HTMLFormElement) {
    const name: string = $form.getAttribute('name');
    const fields = [].map.call($form.elements, $field => new FieldFactory($field));
    console.log(fields);
    // custom functionality
    if (name === 'custom') {
      // $form.onsubmit = e => this.onSubmit(e, fields);
    } else {
      // default functionality
      $form.setAttribute('novalidate', 'true');
      $form.onsubmit = e => this.onDefaultSubmit(e, fields);
    }
    return $form;
  }

  private onDefaultSubmit(e, fields) {
    e.preventDefault();
    const $form: HTMLFormElement = e.target;
    const formData = new FormData($form);
    const action: string = $form.action;
    const $firstErrorField = this.validateFields(fields);
    if ($firstErrorField) {
      $firstErrorField.focus();
    } else {
      console.log('valid!');
      console.log(formData);
      console.log(action);
    }
  }

  private validateFields(fields) {
    return fields.reduce(($fieldEl, field) => {
      field.updateState({
        isPristine: false,
        isValid: field.applyValidators(),
        hasError: !field.applyValidators(),
      });
      return !field.applyValidators() && !$fieldEl ? field.$field : $fieldEl;
    }, null);
  }
}

export default FormFactory;
