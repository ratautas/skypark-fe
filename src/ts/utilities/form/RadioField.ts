import FormField from './FormField';

/**
 * Radio field creation class
 */

class RadioField extends FormField {
  /**
   * A nodelist of sibling radio controls with same name attirbute:
   */
  $radioControls: NodeListOf<HTMLInputElement> = this.$field.form.elements[this.$field.name];

  constructor($field: HTMLInputElement) {
    super($field);
  }

  /**
   * Check if field has value (is checked)
   */
  public hasValue() {
    return this.$field.checked;
  }

  /**
   * Set the state of all siblings' 'hasValue' to false.
   * Update field object's state and start validation.
   */
  public handleChange() {
    // Unset value for all the sibling elements.
    [].forEach.call(this.$radioControls, ($radio: IField) => {
      return $radio.removeValidationRules().updateState({
        hasValue: false,
        isPristine: false,
        hasError: false,
        isValid: true,
      });
    });
    this.updateState({ hasValue: true });
  }
}

export default RadioField;
