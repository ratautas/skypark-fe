/* eslint-disable new-cap */
// import Swiper from 'swiper';
import scrollmonitor from 'scrollmonitor';

export default class Appear {
  isRevealing = false;

  revealQueue = [];

  constructor() {
    this.init();
  }

  init() {
    document.querySelectorAll('[data-appear]:not(.has-entered)').forEach(($el: HTMLElement) => {
      const monitorWatcher = scrollmonitor.create($el);
      this.revealQueue.push($el);
      monitorWatcher.enterViewport(() => {
        this.setReveal();
        monitorWatcher.destroy();
      });
    });
  }

  setReveal() {
    if (!this.isRevealing && this.revealQueue[0]) {
      this.isRevealing = true;
      const $el = this.revealQueue[0];
      const t = String(parseFloat(getComputedStyle($el).transitionDuration));
      const enterTime = t.indexOf('ms') > -1 ? parseFloat(t) : parseFloat(t) * 1000 || 600;
      const enterDelay = $el.dataset.appear ? Number($el.dataset.appear) : 600;

      if (!$el.matches('.has-entered')) {
        $el.classList.add('is-entering');

        setTimeout(() => {
          this.revealQueue.shift();
          this.isRevealing = false;
          this.setReveal();
        }, enterDelay);

        setTimeout(() => {
          $el.classList.remove('is-entering');
          $el.classList.add('has-entered');
        }, enterTime);
      }
    }
  }
}
