interface HTMLPlayerElement extends HTMLElement {
  play: () => void;
  pause: () => void;
}

export default class Player {
  $player: HTMLPlayerElement;
  $playerFile: HTMLVideoElement;
  $playerPlay: HTMLElement;
  isPlaying: boolean = false;
  constructor($player) {
    this.$player = $player;
    this.$playerFile = $player.querySelector('[data-player-file]');
    this.$playerPlay = $player.querySelector('[data-player-play]');
    this.$playerPlay.addEventListener('click', () => this.$playerFile.play());
    this.$playerFile.addEventListener('play', () => {
      this.isPlaying = true;
      this.$player.classList.add('is-playing');
    });
    this.$playerFile.addEventListener('pause', () => {
      this.isPlaying = false;
      this.$player.classList.remove('is-playing');
    });
    this.$playerFile.onclick = () => this.pausePlayer();
    this.playPlayer = this.playPlayer.bind(this);
    this.pausePlayer = this.pausePlayer.bind(this);
    this.$player.play = this.playPlayer;
    this.$player.pause = this.pausePlayer;
    window.addEventListener('scroll', () => this.pausePlayer());
  }

  playPlayer() {
    if (!this.isPlaying) this.$playerFile.play();
  }

  pausePlayer() {
    if (this.isPlaying) this.$playerFile.pause();
  }
}
