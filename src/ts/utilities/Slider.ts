/* eslint-disable new-cap */
// import Swiper from 'swiper';

export default class Slider {
  
  swiperModule: any;

  swiperInstance: any;

  $slider: HTMLElement | Element;

  $target: HTMLElement | Element;

  $next: HTMLElement | Element | null;

  $prev: HTMLElement | Element | null;

  constructor($slider, swiperModule) {
    this.swiperModule = swiperModule;
    this.$slider = $slider;
    this.$target = $slider.querySelector('[data-slider-target]');
    this.$next = $slider.querySelector('[data-slider-next]');
    this.$prev = $slider.querySelector('[data-slider-prev]');
    if (this.$target) this.init();
  }

  init() {
    this.swiperInstance = new this.swiperModule.default(this.$target, {
      loop: true,
      pagination: {
        el: '[data-slider-pagination]',
        type: 'bullets',
        clickable: true,
      },
      speed: 800,
      keyboard: {
        enabled: true,
      },
      autoplay: {
        delay: 5000,
      },
      simulateTouch: false,
      navigation: {
        nextEl: this.$next,
        prevEl: this.$prev,
      },
    });
  }
}
