// SCSS
import '../scss/index.scss';

// POLYFILLS
// Custom POLYFILLS
import './toolkit/polyfills';

// Vendor POLYFILLS and initialization
import 'intersection-observer';
import 'objectFitPolyfill';
import 'whatwg-fetch';
import cssVarsPonyfill from 'css-vars-ponyfill';
cssVarsPonyfill();

// Universal MODULES:
import CookiebarBackend from './modules/CookiebarBackend';
import Player from './modules/Player';
import ScrollTo from './modules/ScrollTo';

import ExpandsController from './modules/expands/ExpandsController';
import MapsController from './modules/maps/MapsController';
import ModalsController from './modules/modals/ModalsController';
import ParallaxsController from './modules/parallaxs/ParallaxsController';
import SlidersController from './modules/sliders/SlidersController';
import VLDController from './modules/vld/VLDController';
import TabsController from './modules/tabs/TabsController';
import TweenersController from './modules/tweeners/TweenersController';

// Custom COMPONENTS:
import Confetti from './components/Confetti';
import Timetable from './components/Timetable';
import Nav from './components/Nav';

// Varios other TOOLS:
import $$ from './toolkit/$$';

const initApp = () => {
  // initialize cookie notification:
  new CookiebarBackend();
  if ($$('[data-player]')[0]) $$('[data-player]').forEach($p => new Player($p));
  new ExpandsController();
  new MapsController();
  new ModalsController();
  new ParallaxsController();
  new ScrollTo();
  new SlidersController();
  new TweenersController();
  new TabsController();
  new VLDController();

  new Confetti();
  new Nav();
  new Timetable();
};

// window.onload = initApp;

window.addEventListener('load', initApp);
