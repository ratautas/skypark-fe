import ModalDefault from './Modal.Default';
import Wizard from '../../components/Wizard';

import TweenerScene from '../../modules/tweeners/Tweener.Scene';

class ModalWizard extends ModalDefault {
  // wizard room ID
  wizardRoom: string;
  // wizard room time
  wizardTime: string;
  // wizard room time
  wizardPrice: string;
  // initialized wizard
  wizard: any;
  // initialized tweener scene
  tweeners: any;
  // target tweener based on room ID
  targetTweener: any;

  handleTriggerClick(e) {
    this.wizardRoom = e.target.dataset.wizardRoom;
    this.wizardTime = e.target.dataset.wizardTime;
    this.wizardPrice = e.target.dataset.wizardPrice;
    this.open();
    return this;
  }

  onTweenerEnd() {
    console.log('funk');
    return this;
  }

  beforeMount() {
    super.beforeMount();
    this.tweeners = [].reduce.call(
      this.$modal.querySelectorAll('[data-wizard-scene]'),
      (scenes, $scene) => {
        const $tweener = $scene.querySelector('[data-tweener]');
        return Object.assign(scenes, {
          [$scene.dataset.wizardScene]: new TweenerScene($tweener, this, 0, 0),
        });
      },
      {},
    );
    return this;
  }

  afterOpen() {
    super.afterOpen();
    if (typeof this.wizardRoom === 'undefined') {
      this.wizardRoom = this.$triggers[0].dataset.wizardRoom;
    }
    if (typeof this.wizardTime === 'undefined') {
      this.wizardTime = this.$triggers[0].dataset.wizardTime;
    }
    if (typeof this.wizardPrice === 'undefined') {
      this.wizardPrice = this.$triggers[0].dataset.wizardPrice;
    }
    this.wizard = new Wizard(this.wizardRoom, this.wizardTime, this.wizardPrice, this);
    const $targetScene = this.$modal.querySelector(`[data-wizard-scene="${this.wizardRoom}"]`);
    $targetScene.classList.add('is-up');
    this.targetTweener = this.tweeners[this.wizardRoom];
    this.targetTweener.timeline.play();

    return this;
  }
}

export default ModalWizard;
