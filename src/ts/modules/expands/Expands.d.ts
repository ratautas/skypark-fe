/**
 * Interface for single [data-epxands-item] HTML element
 * which adds some extra values to an abcject.
 *
 * @export
 * @interface ExpandItem
 * @extends {HTMLElement}
 */

export interface ExpandItem extends HTMLElement {
  ID: string;
  expandTimeline: any;
  $trigger: HTMLElement;
  $content: HTMLElement;
}
