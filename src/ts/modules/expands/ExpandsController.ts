import ExpandsDefault from './Expands.Default';

import $$ from '../../toolkit/$$';
import camelize from '../../toolkit/camelize';

/**
 * Expands initializer factory.
 *
 * @module ExpandsController
 * @requires ExpandsInstance
 */

class ExpandsController {
  /**
   * Creates an instance of ExpandsController.
   *
   * @memberof ExpandsController
   * @returns An object of initialized ExpandsController.
   */

  // A NodeList of '[data-expands]' HTML Elements:
  public $$expands: NodeListOf<HTMLElement>;

  // An array of initialized expands:
  public expands: any[];

  /**
   * Creates an instance of ExpandsController.
   * If there are any `[data-expands]` in DOM, call `mountExpands()`.
   * @memberof ExpandsController
   */
  constructor() {
    this.$$expands = $$('[data-expands]');
    if (this.$$expands[0]) this.mountExpands();
  }

  /**
   * Loops through all `$expands` and depending on type, call Expands constructors.
   *
   * @returns {ExpandsController} For chaining metods
   * @memberof ExpandsController
   */
  mountExpands() {
    this.expands = [].forEach.call(this.$$expands, ($expands, i) => {
      const type = camelize($expands.dataset.expands);
      return new ExpandsDefault($expands, this, i);
    });
    return this;
  }
}

export default ExpandsController;
