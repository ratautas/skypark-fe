import { ExpandItem } from './Expands.d';

class Expands {
  // Expands's items (HTML elements):
  public $$items: NodeListOf<ExpandItem>;
  // Active expand ID:
  public activeID: string;
  // HTML element of the active $triggers:
  public $activeItem: ExpandItem;

  public $expands: HTMLElement;
  public expandsController: any;
  public i: number;

  /**
   * Creates an instance of Expands. Assigns class variables and iniaitate Swiper module.
   * @param {HTMLElement} $expands HTML element of the Expands's target;
   * @param {*} expandsController Expands's parent initialization funtion
   * @param {number} i Expands's initializator's place in array
   * @memberof Expands
   */
  constructor($expands: HTMLElement, expandsController: any, i: number) {
    this.expandsController = expandsController;
    this.i = i;
    this.$expands = $expands;
    this.$$items = $expands.querySelectorAll('[data-expands-item]');
    this.$activeItem = [].find.call(this.$$items, $t => $t.matches('.is-active'));

    this.beforeMount()
      .onMount()
      .afterMount();
  }

  /**
   * Expand checking function.
   *
   * @param {string} id
   * @returns {Expands} For chaining.
   * @memberof Expands
   */
  public handleItem(ID: string, $targetItem: ExpandItem) {
    if (ID !== this.activeID) {
      if (this.$activeItem) this.contractItem(this.$activeItem);
      this.expandItem(ID, $targetItem);
    } else {
      this.contractItem(this.$activeItem);
    }
    return this;
  }

  public beforeContract() {
    return this;
  }
  public onContract($item: ExpandItem) {
    this.afterContract($item);
    return this;
  }
  public afterContract($item: ExpandItem) {
    this.activeID = null;
    this.$activeItem = null;
    $item.classList.remove('is-active');
    return this;
  }

  /**
   * Expanding function.
   *
   * @param {string} id
   * @returns {Expands} For chaining.
   * @memberof Expands
   */
  public expandItem(ID: string, $item: ExpandItem) {
    this.beforeExpand().onExpand(ID, $item);
    return this;
  }

  /**
   * Expand checking function.
   *
   * @param {string} id
   * @returns {Expands} For chaining.
   * @memberof Expands
   */
  public contractItem($item: ExpandItem) {
    this.beforeContract().onContract($item);
    return this;
  }

  /**
   * Extra function to call before initialization. Useful for extending.
   *
   * @returns {Expands} For chaining.
   * @memberof Expands
   */
  public beforeExpand() {
    return this;
  }

  /**
   * Default expand open function. Use it by overriding or extending i.e. for animations.
   *
   * @param {string} ID Target item's slug
   * @param {ExpandItem} $targetItem Target item's HTML element
   * @returns {Expands} For chaining.
   * @memberof Expands
   */
  public onExpand(ID: string, $targetItem: ExpandItem) {
    this.afterExpand(ID, $targetItem);
    return this;
  }

  /**
   * Extra function to call after initialization. Useful for extending.
   *
   * @param {string} ID Target item's slug
   * @param {ExpandItem} $targetItem Target item's HTML element
   * @returns {Expands} For chaining.
   * @memberof Expands
   */
  public afterExpand(ID: string, $targetItem: ExpandItem) {
    this.activeID = ID;
    this.$activeItem = $targetItem;
    $targetItem.classList.add('is-active');
    if (typeof this.activeID !== 'undefined') location.hash = `#${this.activeID}`;
    return this;
  }

  /**
   * Default expands mount function. Use it by overriding or extending.
   *
   * @returns {Expands} For chaining.
   * @memberof Expands
   */
  public onMount() {
    return this;
  }

  /**
   * Extra function to call before initialization. Useful for extending.
   *
   * @returns {Expands} For chaining.
   * @memberof Expands
   */
  public beforeMount() {
    [].forEach.call(this.$$items, ($item: ExpandItem) => {
      $item.ID = $item.dataset.expandsItem;
      $item.$trigger = $item.querySelector('[data-expands-trigger]');
      $item.$content = $item.querySelector('[data-expands-content]');
      $item.$trigger.addEventListener('click', () => this.handleItem($item.ID, $item));
    });
    return this;
  }

  /**
   * Extra function to call after initialization. Useful for extending.
   *
   * @returns {Expands} For chaining.
   * @memberof Expands
   */
  public afterMount() {
    this.checkForID();
    return this;
  }

  /**
   * Check URL for expands's ID and opens it, if found.
   *
   * @returns {Expands} For chaining.
   * @memberof Expands
   */
  public checkForID() {
    // Exctract needed strings.
    const { hash } = window.location;
    // Get ID from hash
    const ID = hash.replace('#', '');
    // Search through all the expands with '.find()', to find matching 'id'.
    const $targetItem = [].find.call(this.$$items, $t => $t.dataset.expandsItem === ID);
    // If target expand was found, open it.
    if ($targetItem) this.handleItem(ID, $targetItem);

    return this;
  }
}

export default Expands;
