import animejs from 'animejs';

import { ExpandItem } from './Expands.d';

import Expands from './Expands';

class ExpandsDefault extends Expands {
  public beforeMount() {
    [].forEach.call(this.$$items, ($item: ExpandItem) => {
      $item.ID = $item.dataset.expandsItem;
      $item.$trigger = $item.querySelector('[data-expands-trigger]');
      $item.$content = $item.querySelector('[data-expands-content]');
      $item.$trigger.addEventListener('click', () => this.handleItem($item.ID, $item));
      $item.$content.style.display = 'block';
      $item.$content.style.overflow = 'hidden';

      $item.expandTimeline = animejs.timeline({ autoplay: false }).add({
        targets: $item.$content,
        height: [0, $item.$content.clientHeight],
        easing: 'easeOutQuad',
        duration: 500,
        complete: () => this.afterExpand($item.ID, $item),
      });
    });

    return this;
  }

  public onExpand(ID: string, $targetItem: ExpandItem) {
    $targetItem.expandTimeline.play();

    return this;
  }

  public onContract($item: ExpandItem) {
    this.afterContract($item);

    animejs({
      targets: $item.$content,
      height: 0,
      easing: 'easeOutQuad',
      duration: 300,
    });

    return this;
  }
}

export default ExpandsDefault;
