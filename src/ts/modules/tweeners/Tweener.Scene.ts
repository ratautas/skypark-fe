import animejs from 'animejs';

import TweenerDefault from './Tweener.Default';

import Heading from '../../components/Heading';

class TweenerScene extends TweenerDefault {
  // both background and foreground SVG shapes
  $$shapes: NodeListOf<HTMLElement>;
  // scene image
  $image: HTMLElement;
  // scene HTML heading element
  $heading: HTMLElement;
  // initailized heiding animation with Heading constructor
  initHeading: any = null;

  public beforeMount() {
    this.$$shapes = this.$tweener.querySelectorAll('[data-tweener-shape]');
    this.$image = this.$tweener.querySelector('[data-tweener-image]');
    this.$heading = this.$tweener.querySelector('[data-tweener-heading]');
    this.initHeading = this.$heading ? new Heading(this.$heading) : null;
    this.$tweener.classList.remove('_before-tween');
    this.$tweener.removeAttribute('data-tweener');
    return this;
  }

  public addTweens() {
    const offset: number = this.$tweener.clientHeight * 1.5;

    if (this.$$shapes[0]) {
      // [].forEach.call(this.$$shapes, $shape => ($shape.style.willChange = 'transform'));
      this.timeline.add({
        targets: this.$$shapes,
        translateY: [offset, 0],
        easing: 'spring(2, 80, 10, 0)',
        complete: () => {
          animejs({
            easing: 'easeInQuad',
            targets: this.$$shapes,
            translateY: 10,
            duration: 2500,
            direction: 'alternate',
            loop: true,
          });
        },
      });
    }

    if (this.$image) {
      // this.$image.style.willChange = 'transform';
      this.timeline.add(
        {
          targets: this.$image,
          translateY: [offset - 30, 0],
          easing: 'spring(2, 80, 10, 0)',
          complete: () => {
            animejs({
              easing: 'easeInCubic',
              targets: this.$image,
              translateY: 20,
              duration: 2500,
              direction: 'alternate',
              loop: true,
            });
          },
        },
        20,
      );
    }
    if (this.initHeading && this.initHeading.symbolAnimations) {
      this.initHeading.symbolAnimations.forEach(anim =>
        this.timeline.add({ ...anim }, this.suspend),
      );
    }
    return this;
  }
}

export default TweenerScene;
