import Tweener from './Tweener';

class TweenerDefault extends Tweener {
  public addTweens() {
    this.timeline.add({
      targets: this.$tweener,
      translateY: [60, 0],
      opacity: [0, 1],
      duration: 500,
      easing: 'spring(1, 50, 10, 0)',
    });
    return this;
  }

  public onTimelineComplete() {
    super.onTimelineComplete();
    this.$tweener.classList.remove('_should-tween');
    return this;
  }
}

export default TweenerDefault;
