import Tabs from './Tabs';

class TabsDefault extends Tabs {
  public onOpen(id: string) {
    const $targetTrigger = [].find.call(this.$$triggers, $t => $t.dataset.tabsTrigger === id);
    const $targetContent = [].find.call(this.$$contents, $c => $c.dataset.tabsContent === id);
    if ($targetContent && $targetTrigger && this.$activeTrigger !== $targetTrigger) {
      this.$activeTrigger.classList.remove('is-active');
      this.$activeContent.classList.remove('is-active');
      // TweenMax.to(this.$activeContent, 0.8, {
      //   y: -20,
      //   opacity: 0,
      //   scale: 0.8,
      //   ease: Power4.easeOut,
      // });

      $targetTrigger.classList.add('is-active');
      $targetContent.classList.add('is-active');

      // TweenMax.fromTo(
      //   $targetContent,
      //   0.8,
      //   {
      //     y: -60,
      //     opacity: 0,
      //     scale: 1,
      //   },
      //   {
      //     y: 0,
      //     opacity: 1,
      //     scale: 1,
      //     ease: Power4.easeOut,
      //     onComplete: () => {},
      //   },
      // );

      this.activeID = id;
      this.$activeTrigger = $targetTrigger;
      this.$activeContent = $targetContent;
    }
    return this;
  }
}

export default TabsDefault;
