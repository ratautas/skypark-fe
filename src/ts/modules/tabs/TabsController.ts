import TabsDefault from './Tabs.Default';

import $$ from '../../toolkit/$$';
import camelize from '../../toolkit/camelize';

/**
 * Tabs initializer factory.
 *
 * @module TabsController
 * @requires TabsInstance
 */

class TabsController {
  /**
   * Creates an instance of TabsController.
   *
   * @memberof TabsController
   * @returns An object of initialized TabsController.
   */

  // A NodeList of '[data-tabs]' HTML Elements:
  public $tabs: NodeListOf<HTMLElement>;

  // An array of initialized tabs:
  public tabs: any[];

  /**
   * Creates an instance of TabsController.
   * If there are any `[data-tabs]` in DOM, call `mountTabs()`.
   * @memberof TabsController
   */
  constructor() {
    this.$tabs = $$('[data-tabs]');
    if (this.$tabs[0]) this.mountTabs();
  }

  /**
   * Loops through all `$tabs` and depending on type, call Tabs constructors.
   *
   * @returns {TabsController} For chaining metods
   * @memberof TabsController
   */
  mountTabs() {
    this.tabs = [].forEach.call(this.$tabs, ($tabs, i) => {
      const type = camelize($tabs.dataset.tabs);
      return new TabsDefault($tabs, this, i);
    });
    return this;
  }
}

export default TabsController;
