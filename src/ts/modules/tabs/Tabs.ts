class Tabs {
  // Initialized tabs
  public tabs: any;
  // HTML element of Tabs's triggers;
  public $$triggers: NodeListOf<HTMLElement>;
  // HTML element of Tabs's contents;
  public $$contents: NodeListOf<HTMLElement>;
  // Active tab ID
  public activeID: string;
  // HTML element of the active $triggers;
  public $activeTrigger: HTMLElement;
  // HTML element of the active $content;
  public $activeContent: HTMLElement;

  public $tabs: HTMLElement;
  public tabsController: any;
  public i: number;

  /**
   * Creates an instance of Tabs. Assigns class variables and iniaitate Swiper module.
   * @param {HTMLElement} $tabs HTML element of the Tabs's target;
   * @param {*} tabsController Tabs's parent initialization funtion
   * @param {number} i Tabs's initializator's place in array
   * @memberof Tabs
   */
  constructor($tabs: HTMLElement, tabsController: any, i: number) {
    this.tabsController = tabsController;
    this.i = i;
    this.$tabs = $tabs;
    this.$$contents = $tabs.querySelectorAll('[data-tabs-content]');
    this.$$triggers = $tabs.querySelectorAll('[data-tabs-trigger]');
    this.$activeTrigger = [].find.call(this.$$triggers, $t => $t.matches('.is-active'));
    this.$activeContent = [].find.call(this.$$contents, $c => $c.matches('.is-active'));

    this.beforeMount()
      .onMount()
      .afterMount()
      .checkForID();
  }

  /**
   * Add event listeners.
   *
   * @returns {Tabs} For chaining.
   * @memberof Tabs
   */
  public attachEventListeners() {
    [].forEach.call(this.$$triggers, ($trigger: HTMLElement) => {
      $trigger.addEventListener('click', () => this.openByID($trigger.dataset.tabsTrigger));
    });
    return this;
  }

  /**
   * Tab open function.
   *
   * @param {string} id
   * @returns {Tabs} For chaining.
   * @memberof Tabs
   */
  public openByID(id: string) {
    this.beforeOpen()
      .onOpen(id)
      .afterOpen();
    return this;
  }

  /**
   * Extra function to call before initialization. Useful for extending.
   *
   * @returns {Tabs} For chaining.
   * @memberof Tabs
   */
  public beforeOpen() {
    return this;
  }

  /**
   * Default tab open function. Use it by overriding or extending i.e. for animations.
   *
   * @returns {Tabs} For chaining.
   * @memberof Tabs
   */
  public onOpen(id: string) {
    const $targetTrigger = [].find.call(this.$$triggers, $t => $t.dataset.tabsTrigger === id);
    const $targetContent = [].find.call(this.$$contents, $c => $c.dataset.tabsContent === id);
    if ($targetContent && $targetTrigger && this.$activeTrigger !== $targetTrigger) {
      this.$activeTrigger.classList.remove('is-active');
      this.$activeContent.classList.remove('is-active');
      $targetTrigger.classList.add('is-active');
      $targetContent.classList.add('is-active');
      this.activeID = id;
      this.$activeTrigger = $targetTrigger;
      this.$activeContent = $targetContent;
    }
    return this;
  }

  /**
   * Extra function to call after initialization. Useful for extending.
   *
   * @returns {Tabs} For chaining.
   * @memberof Tabs
   */
  public afterOpen() {
    if (typeof this.activeID !== 'undefined') location.hash = `#${this.activeID}`;
    return this;
  }

  /**
   * Default tabs mount function. Use it by overriding or extending.
   *
   * @returns {Tabs} For chaining.
   * @memberof Tabs
   */
  public onMount() {
    this.attachEventListeners();
    return this;
  }

  /**
   * Extra function to call before initialization. Useful for extending.
   *
   * @returns {Tabs} For chaining.
   * @memberof Tabs
   */
  public beforeMount() {
    return this;
  }

  /**
   * Extra function to call after initialization. Useful for extending.
   *
   * @returns {Tabs} For chaining.
   * @memberof Tabs
   */
  public afterMount() {
    return this;
  }

  /**
   * Check URL for tabs's ID and opens it, if found.
   *
   * @returns {Tabs} For chaining.
   * @memberof Tabs
   */
  public checkForID() {
    // Exctract needed strings.
    const { hash } = window.location;

    const id = hash.replace('#', '');
    // Search through all the tabs with '.find()', to find matching 'id'.
    const targetTab = [].find.call(this.$$triggers, $t => $t.dataset.tabsTrigger === id);
    // If target tab was found, open it.
    if (targetTab) this.openByID(id);

    return this;
  }
}

export default Tabs;
