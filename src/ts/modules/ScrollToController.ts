import $$ from '../toolkit/$$';
import scrollTo from '../toolkit/scrollTo';

class ScrollToController {
  // ScrollTo click listenrers
  public $$triggers: NodeListOf<HTMLElement> = $$('[data-scrollto-trigger]');

  // ScrollTo target with hashes
  public $$targets: NodeListOf<HTMLElement> = $$('[data-scrollto]');

  // ScrollTo target with hashes
  public $offsetElement: HTMLElement = $$('[data-scrollto-offset-element]')[0];

  public offset: number = 0;

  // base url.
  public baseUrl: string = document.body.getAttribute('data-base');

  public options: {
    [option: string]: any;
  };

  /**
   *Creates an instance of ScrollToController.
   * @param {*} [options]
   * @memberof ScrollToController
   */
  constructor(options?: any) {
    this.options = options ? { urlMode: 'hash', ...options } : { urlMode: 'hash' };
    if (this.$offsetElement) this.offset = this.$offsetElement.clientHeight;
    if (this.$$triggers[0]) {
      [].forEach.call(this.$$triggers, ($trigger: HTMLElement) => {
        const $target = $$($trigger.dataset.scrolltoTrigger)[0];
        if ($target) {
          $trigger.addEventListener('click', () => {
            this.doScroll($target, 500);
          });
        }
      });
    }
    this.checkForScrollTo();
  }

  /**
   *
   *
   * @param {string} id
   * @memberof ScrollToController
   */
  setActiveTrigger(id: string) {
    const $trigger = $$(`[data-scrollto-trigger="${id}"]`)[0];
    if ($trigger) {
      [].forEach.call(this.$$triggers, ($t: HTMLElement) => {
        $t.classList[$t === $trigger ? 'add' : 'remove']('is-active');
        $t.parentElement.classList[$t === $trigger ? 'add' : 'remove']('is-active');
      });
    }
  }

  /**
   *
   *
   * @param {HTMLElement} $target
   * @param {number} [speed=500]
   * @memberof ScrollToController
   */
  doScroll($target: HTMLElement, speed: number = 500) {
    const scrollPos =
      document.documentElement.scrollTop ||
      document.body.parentNode['scrollTop'] ||
      document.body.scrollTop;
    const id = $target.dataset.scrollto;
    const offset = $target.dataset.scrolltoOffset
      ? Number($target.dataset.scrolltoOffset)
      : this.offset;
    const fromTop = $target.getBoundingClientRect().top - offset + scrollPos;

    scrollTo(fromTop, speed, () => {
      if (id) {
        this.updateUrl(id);
        this.setActiveTrigger(id);
      }
    });
  }

  /**
   *
   *
   * @returns
   * @memberof ScrollToController
   */
  checkForScrollTo() {
    // Exctract needed strings.
    let { href } = window.location;
    const { hash } = window.location;
    // If the last symobol of current URL is '/', remove it.
    if (href[href.length - 1] === '/') href = href.slice(0, -1);
    // If urlMode is 'href', get the last piece of current URL from '/'.
    // Else, if it is hash, just remove '#'.
    const slug = this.options.urlMode === 'href' ? href.split('/').pop() : hash.replace('#', '');
    // Search throught all the $$targets with '.find()', to find matching 'id'.
    const $target = [].find.call(this.$$targets, $target => $target.dataset.scrollto === slug);
    // If target was found, scroll to it.
    if ($target) this.doScroll($target, 500);

    return this;
  }

  /**
   * Update URL after scroll is done.
   *
   * @returns {ScrollToController} For chaining.
   * @memberof ScrollToController
   */
  private updateUrl(id) {
    if (this.options.urlMode === 'href') {
      // If URL mode is set to 'href', use history API.
      history.pushState(null, null, `${this.baseUrl}/${id}`);
    } else if (this.options.urlMode === 'hash') {
      // If URL mode is NOT set to set to 'hash', add hash.
      location.hash = `#${id}`;
    }
    return this;
  }
}

export default ScrollToController;
