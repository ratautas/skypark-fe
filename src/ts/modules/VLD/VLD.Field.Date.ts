import flatpickr from 'flatpickr';

import VLDField from './VLD.Field';

class VLDFieldDate extends VLDField {
  // initialiazed flatpickr object
  datePicker: any;
  /**
   * Before proceeding with `mountField()` method, initialize `flatpickr`library
   * on HTML `<input type="date">` with some default options.
   *
   * @returns {VLDFieldDate} For chaining.
   * @memberof VLDFieldDate
   */

  public beforeMount(): VLDFieldDate {
    flatpickr.l10ns.default.firstDayOfWeek = 1; // Monday

    this.datePicker = flatpickr(this.$field, {
      inline: true,
      closeOnSelect: true,
      monthSelectorType: 'static',
      prevArrow: '&larr;',
      nextArrow: '&rarr;',
      onMonthChange: () => setTimeout(() => this.handleFocus(), 0),
    });
    return this;
  }
}

export default VLDFieldDate;
