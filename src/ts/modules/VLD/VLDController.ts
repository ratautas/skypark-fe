import { IVLDMountedForms, IVLDForm } from './VLD';

import VLDForm from './VLD.Form';

/**
 * VLDController form validation and state management module.
 *
 * @module VLDController
 * @requires VLDForm
 * @requires VLDFormCustom // <-- if using extended form handling
 */

class VLDController {
  /**
   * Creates an instance of VLDController.
   *
   * @param {NodeListOf<HTMLFormElement>} $forms A NodeList of <form> elements
   * @memberof VLDController
   * @returns An object of instantiated forms, keyed by their names.
   */

  // constructor($forms: NodeListOf<HTMLFormElement>) {
  constructor() {
    // Loop with .reduce() through all the <form> elements.
    // return [].reduce.call(
    //   document.forms,
    //   (acc: IVLDMountedForms, $form: HTMLFormElement) => {
    //     const name: string = $form.getAttribute('name');
    //     return Object.assign(acc, { [name]: this.mount($form, name) });
    //   },
    //   {}, // <-- Start reducer with an epmty accumulation object.
    // );
    return [].filter
      .call(document.forms, $form => $form.name !== 'wizard')
      .reduce(
        (acc: IVLDMountedForms, $form: HTMLFormElement) => {
          const name: string = $form.getAttribute('name');
          return Object.assign(acc, { [name]: this.mount($form, name) });
        },
        {}, // <-- Start reducer with an epmty accumulation object.
      );
  }

  /**
   * Create custom form initializations by passing a custom name to this method.
   *
   * @private
   * @param {HTMLFormElement} $form
   * @param {string} name
   * @returns Instantiated form constructor
   * @memberof VLDController
   */
  private mount($form: HTMLFormElement, name: string) {
    // if (name === 'request') return new VLDFormRequest($form) as IVLDForm;
    return new VLDForm($form) as IVLDForm; // <-- Default initiaizition.
  }
}

export default VLDController;
