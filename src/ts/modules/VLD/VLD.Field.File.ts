import VLDField from './VLD.Field';

class VLDFieldFile extends VLDField {
  // file name HTML conainer
  $filename: HTMLElement = null;

  public handleChange() {
    const file = this.$field.files[0];
    if (!this.$filename) {
      this.$filename = this.$field.parentElement.querySelector('[data-field-filename]');
    }
    this.$filename.innerText = file ? file.name : '';
    this.updateState({
      hasValue: this.hasValue(),
      isValid: this.applyValidators(),
      hasError: !this.applyValidators() && !this.$field.state.isPristine,
    });
    !file && setTimeout(() => this.handleBlur(), 0);
  }
}

export default VLDFieldFile;
