import { IVLDForm } from './VLD.d';

import VLDField from './VLD.Field';
import VLDFieldCheckBox from './VLD.Field.CheckBox';
import VLDFieldDate from './VLD.Field.Date';
import VLDFieldEmail from './VLD.Field.Email';
import VLDFieldFile from './VLD.Field.File';
import VLDFieldRadio from './VLD.Field.Radio';
import VLDFieldSelect from './VLD.Field.Select';

/**
 * `VLDForm` module for managing form state and actions.
 *
 * @module VLDForm
 * @requires VLDFieldCheckBox // <-- Custom imports for custom field types.
 */

class VLDForm implements IVLDForm {
  $form;
  vldFields;

  /**
   * Creates an instance of `VLDForm`.
   * Assign `<form>` element to constructor and mount automatically if it's not set otherwise.
   *
   * @param {HTMLFormElement} $form Rquires a `<form>` element as the main param.
   * @param {boolean} [autoMount=true] Optional param to stop automatic mount on initialization.
   * @memberof VLDForm
   */
  constructor($form: HTMLFormElement, autoMount: boolean = true) {
    this.$form = $form;
    if (autoMount) this.mountFields();
  }

  /**
   * Instantiate all the fields via `mountField()` method with reducer for each field.
   * Trigger `beforeFieldsMount()` just before `.map()`.
   * Trigger `afterFieldsMount()` just after `.map()`.
   * These extra methods could be hooked into when extending base functionality.
   * This method is public to provide external access.
   *
   * @returns {VLDField} For chaining.
   * @memberof VLDForm
   */
  public mountFields() {
    // Fire beforeFieldsMount method first, before reducer:
    this.beforeFieldsMount();

    // Loop through all `<form>` elements and create new `IVLDField` object for every element.
    this.vldFields = [].filter
      // omit flatpikcr's year input:
      .call(this.$form.elements, $field => !$field.matches('.numInput.cur-year'))
      .map($field => this.mountField($field));

    // Fire after .filter() and .map() methods, when all the fields are already initialized:
    this.afterFieldsMount();

    return this;
  }

  /**
   * Initialize `VLDField` functionality with state handling and validation according to their type.
   * If no particullar type is provided, return default new `VLDField()`.
   *
   * @public
   * @param {HTMLInputElement} $field
   * @returns `IVLDField` object with binded events, state and it's method.
   * @memberof VLDForm
   */
  public mountField($field: HTMLInputElement) {
    if ($field.type === 'checkbox') return new VLDFieldCheckBox($field, this);
    // if ($field.type === 'date') return new VLDFieldDate($field, this);
    if ($field.type === 'email') return new VLDFieldEmail($field, this);
    // if ($field.type === 'file') return new VLDFieldFile($field, this);
    if ($field.type === 'radio') return new VLDFieldRadio($field, this);
    if ($field.type === 'select-one') return new VLDFieldSelect($field, this);
    return new VLDField($field, this); // <-- default return
  }

  /**
   * Call just before field initializtion to add extra functionality.
   * Currently it prevents native validation and passes submit event to this.submit().
   * It can be either extended or overriden.
   *
   * @memberof VLDForm
   */
  public beforeFieldsMount() {
    this.$form.setAttribute('novalidate', 'true');
    // this.$form.onsubmit = event => this.onSubmit(event);
    // this.$form.addEventListe
  }

  /**
   * Function call all just after field initializtion to add extra functionality.
   *
   * @memberof VLDForm
   */
  public afterFieldsMount() {}

  /**
   * Function call just before form submission.
   * Currently it prevents default form submission, but can be extended or overriden.
   *
   * @param {Event} event
   * @memberof VLDForm
   */
  public beforeSubmit(event: Event) {
    event.preventDefault();
  }

  /**
   * Form submission event handler.
   * It fires `beforeSubmit()` funtion for extra actions.
   * Then it tries to find first focusable field with error and call `.focus()` it.
   * If it cannot find focusable error field, it means the form isnt actually valid.
   * Do the actual submission!
   *
   * @public
   * @param {Event} event
   * @memberof VLDForm
   */
  public onSubmit(event: Event) {
    this.beforeSubmit(event);
    const $errorField = this.getErrorField();
    if ($errorField) {
      $errorField.focus();
    } else {
      fetch(this.$form.action, { method: 'POST', body: new FormData(this.$form) })
        // .then(res => res.json())
        .then(res => res.text())
        .catch(err => this.onResponseError(err))
        .then(response => this.onResponseSuccess(response));
    }
  }

  /**
   * Error handling.
   *
   * @public
   * @memberof VLDForm
   */

  public onResponseError(error) {
    console.log('Response error:', error);
  }

  /**
   * Success handling.
   *
   * @public
   * @memberof VLDForm
   */

  public onResponseSuccess(res) {
    this.$form.classList.add(res === 'success' ? 'has-success' : 'has-error');
  }

  /**
   * Loop through all whe fields with `.reduce()`.
   * Get each field's validation status with `applyValidators()` and update the state.
   * If the field is not valid ant no error field is set (`null` as defaut), set it to
   * to target field.
   *
   * @public
   * @returns First field
   * @memberof VLDForm
   */
  public getErrorField() {
    return this.vldFields.reduce(($fieldEl, field) => {
      field.updateState({
        isPristine: false,
        isValid: field.applyValidators(),
        hasError: !field.applyValidators(),
      });
      return (!field.applyValidators() && !$fieldEl ? field.$field : $fieldEl) as boolean;
    }, null);
  }
}

export default VLDForm;
