import Slider from './Slider';

class SliderGallery extends Slider {
  public updateOptions() {
    return {
      pagination: {
        el: '[data-slider-pagination]',
        type: 'bullets',
        clickable: true,
      },
      autoplay: {
        delay: 5000,
      },
    };
  }
}

export default SliderGallery;
