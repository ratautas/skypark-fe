// import { TweenMax, Power2 } from 'gsap/all';
import animejs from 'animejs';

import * as Cookies from 'js-cookie';

import $$ from '../toolkit/$$';

class CookieBar {
  $bar: HTMLElement;
  $accepts: NodeListOf<HTMLElement>;
  hasCookie: boolean;
  animation: boolean;
  constructor() {
    if ($$('[data-cookiebar]')[0]) {
      this.$bar = $$('[data-cookiebar]')[0];
      this.$accepts = $$('[data-cookiebar-accept]');
      this.hasCookie = Boolean(Cookies.get('cookiebar'));
      this.setCookie = this.setCookie.bind(this);
      setTimeout(() => this.checkCookie(), 4000);
    }
    return;
  }

  checkCookie() {
    if (this.hasCookie) {
      this.$bar.parentElement.removeChild(this.$bar);
    } else {
      document.body.classList.add('has-cookie');
      this.$bar.classList.add('is-active');
      animejs.set(this.$bar, { display: 'block' });
      animejs({
        targets: this.$bar,
        duration: 500,
        translateY: ['150%', 0],
        easing: 'spring(1, 50, 5, 0)',
      });
    }
    if (this.$accepts) [].forEach.call(this.$accepts, $a => ($a.onclick = this.setCookie));
  }

  setCookie() {
    Cookies.set('cookiebar', 1);
    document.body.classList.remove('has-cookie');
    this.$bar.classList.remove('is-active');
    animejs({
      targets: this.$bar,
      duration: 750,
      translateY: '150%',
      complete: () => this.$bar.parentElement.removeChild(this.$bar),
    });
  }
}

export default CookieBar;
