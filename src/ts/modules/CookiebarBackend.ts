import animejs from 'animejs';

import $$ from '../toolkit/$$';

class Cookiebar {
  $bar: HTMLElement;
  $accepts: NodeListOf<HTMLElement>;
  animation: boolean;
  endpoint: string;
  constructor() {
    this.setCookie = this.setCookie.bind(this);
    this.$bar = $$('[data-cookiebar]')[0];
    this.$accepts = $$('[data-cookiebar-accept]');
    this.setCookie = this.setCookie.bind(this);
    if (this.$bar) this.showCookie();
    return;
  }

  showCookie() {
    this.endpoint = this.$bar.dataset.cookiebar;
    this.$bar.classList.add('is-active');
    document.body.classList.add('has-cookie');
    animejs.set(this.$bar, { display: 'block' });
    animejs({
      targets: this.$bar,
      translateY: ['150%', 0],
      duration: 500,
      easing: 'spring(1, 50, 5, 0)',
    });
    if (this.$accepts) [].forEach.call(this.$accepts, $a => ($a.onclick = this.setCookie));
  }

  setCookie() {
    fetch(this.endpoint)
      .then((r: Response) => r.text())
      .then((response: any) => {
        if (response === 'success') {
          animejs({
            targets: this.$bar,
            duration: 750,
            translateY: '150%',
            complete: () => this.$bar.parentElement.removeChild(this.$bar),
          });
        } else {
          console.log('error');
        }
      })
      .catch(error => console.log(error));
  }
}

export default Cookiebar;
