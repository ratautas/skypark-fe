export default function getTransitionDuration($el: Element | HTMLElement) {
  const prop = [
    'transitionDuration',
    'WebkitTransitionDuration',
    'msTransitionDuration',
    'MozTransitionDuration',
    'OTransitionDuration',
  ].find(p => typeof ($el as HTMLElement).style[p] !== 'undefined');
  const duration: string = window.getComputedStyle($el)[prop];
  return duration.indexOf('ms') > -1 ? parseFloat(duration) : parseFloat(duration) * 1000;
}
