<?php include './partials/_head.php'; ?>

<div class="app__page app__page--default default">
  
  <div class="default__intro">

    <header class="default__header header header--white">
      <?php include './partials/header--white.php'; ?>
    </header>

    <?php $heading = 'PRIVACY<br>POLICY'; include './partials/scenes/default__scene--1.php'; ?>

  </div>

  <main class="default__content">

    <header class="default__header header header--black">
      <?php include './partials/header--black.php'; ?>
    </header>

    <section class="default__section default__section--wysiwyg _wysiwyg _before-tween" data-tweener>
      <div class="default__container default__container--wysiwyg container">
        <div class="default__wysiwyg _wysiwyg">
          <h3>Introduction</h3>
          <p>This page informs you of our policies regarding the collection, use, and disclosure of
            personal data when you use our Service and the choices you have associated with that
            data. Our Privacy Policy for Skypark is managed through
            https://www.freeprivacypolicy.com/free-privacy-policy-generator.php</p>
          <p>We use your data to provide and improve the Service. By using the Service, you agree to
            the collection and use of information in accordance with this policy. Unless otherwise
            defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings
            as in our Terms and Conditions, accessible from htttp://www.ark.io</p>
          <h3>Information Collection And Use</h3>
          <p>We collect several different types of information for various purposes to provide and
            improve our Service to you</p>
          <h3>Types of data collected</h3>
          <h4>Personal Data</h4>
          <p>While using our Service, we may ask you to provide us with certain personally
            identifiable information that can be used to contact or identify you (“Personal Data”).
            Personally identifiable information may include, but is not limited to:</p>
          <ul>
            <li>Email address</li>
            <li>First name and last name</li>
            <li>Cookies and Usage Data</li>
          </ul>
          <h4>Usage Data</h4>
          <p>We may also collect information how the Service is accessed and used (“Usage Data”).
            This Usage Data may include information such as your computer’s Internet Protocol
            address (e.g. IP address), browser type, browser version, the pages of our Service that
            you visit, the time and date of your visit, the time spent on those pages, unique device
            identifiers and other diagnostic data.</p>
          <h4>Tracking & Cookies Data</h4>
          <p>We use cookies and similar tracking technologies to track the activity on our Service
            and hold certain information.</p>
          <p>Cookies are files with small amount of data which may include an anonymous unique
            identifier. Cookies are sent to your browser from a website and stored on your device.
            Tracking technologies also used are beacons, tags, and scripts to collect and track
            information and to improve and analyze our Service.</p>
          <p>You can instruct your browser to refuse all cookies or to indicate when a cookie is
            being sent. However, if you do not accept cookies, you may not be able to use some
            portions of our Service.</p>
        </div>
      </div>
    </section>

  </main>

</div>

<?php include './partials/_foot.php';
