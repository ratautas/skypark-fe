<?php include './partials/_head.php'; ?>

<div class="app__page app__page--contacts contacts">

  <header class="contacts__header header header--white">
    <?php include './partials/header--white.php'; ?>
  </header>

  <?php $heading="hello@skypark.lt"; include './partials/scenes/contacts__scene.php'; ?>

  <a class="contacts__phone _before-tween" data-tweener data-tweener-suspend="250" href="tel:+370 650 09391">+370 650 09391</a>

  <div class="contacts__info">
    <div class="contacts__row _before-tween" data-tweener data-tweener-suspend="750">
      <div class="contacts__entry">Verkių g. 29, Ogmios miestas Šeimos aikštelė 3 Vilnius</div>
    </div>
    <div class="contacts__row _before-tween" data-tweener data-tweener-suspend="500">
      <div class="contacts__entry">I-V 12:00 - 22:00.</div>
      <div class="contacts__entry">VI-VII 10:00 - 22:00</div>
    </div>
  </div>

  <div class="contacts__actions _before-tween" data-tweener data-tweener-suspend="1000">
    <div class="contacts__action btn btn--white" data-modal-trigger="map">
      <span class="btn__text">VIEW IT ON MAP</span>
    </div>
  </div>

</div>

<?php include './partials/modals/map.php'; ?>

<?php include './partials/_foot.php';
