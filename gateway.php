<?php include './partials/_head.php'; ?>

<div class="app__page app__page--gateway gateway">

  <div class="gateway__logo">
    <?php include './assets/svg/logo.svg'; ?>
  </div>

  <div class="gateway__blocks">

    <div class="gateway__block gateway__block--teal">
      <?php $heading = 'VILNIUS'; include './partials/scenes/gateway__scene--1.php'; ?>
      <a class="gateway__trigger" href="index"></a>
    </div>

    <div class="gateway__block gateway__block--yellow">
      <?php $heading = 'KLAIPĖDA'; include './partials/scenes/gateway__scene--2.php'; ?>
      <a class="gateway__trigger" href="index"></a>
    </div>

  </div>

  <?php include './partials/_foot.php';
