<?php include './partials/_head.php'; ?>

<div class="app__page app__page--default default">
  <div class="default__intro">

    <header class="default__header header header--white">
      <?php include './partials/header--white.php'; ?>
    </header>
    
    <?php $heading = 'PRICE LIST'; include './partials/scenes/default__scene--1.php'; ?>

  </div>

  <main class="default__content">

    <header class="default__header header header--black">
      <?php include './partials/header--black.php'; ?>
    </header>

    <section class="default__section default__section--pricing">
      <div class="default__container default__container--pricing container">
        <div class="default__pricing pricing">
          <div class="pricing__prices">

            <div class="pricing__block _before-tween" data-tweener>
              <div class="pricing__price">
                <div class="pricing__value">8,2 €</div>
                <div class="pricing__time">MONDAY-THURSDAY</div>
                <div class="pricing__label">Ticket price</div>
              </div>
            </div>

            <div class="pricing__block _before-tween" data-tweener>
              <div class="pricing__price">
                <div class="pricing__value">9,8 €</div>
                <div class="pricing__time">FRIDAY-SUNDAY</div>
                <div class="pricing__label">Ticket price</div>
              </div>
            </div>

            <div class="pricing__block _before-tween" data-tweener>
              <div class="pricing__price">
                <div class="pricing__value">5,2 €</div>
                <div class="pricing__time">AFTER 20:00 H</div>
                <div class="pricing__label">Everyday</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="default__section default__section--details">
      <div class="default__details details">
        <div class="details__block _before-tween" data-tweener>
          <div class="details__heading" data-heading="watch">SPECIAL OFFERS</div>
          <div class="details__text">
            <ul>
              <li>Grupėms nuo 8 asmenų taikome
                <b>20% nuolaidą</b>
                dienos bilietui.</li>
              <li>Moksleivių grupėms rezervavus laiką iš anksto
                <b>2,5 val. kaina mokiniui 5 €</b>. Moksleivių grupes priimam darbo dienomis iki
                16.00 val.</li>
            </ul>
          </div>
        </div>
        <div class="details__block _before-tween" data-tweener>
          <div class="details__heading" data-heading="watch">MAIN INFORMATION</div>
          <div class="details__text">
            <p>Norėdami daugiau sužinoti ar rezervuoti laiką skambinkite telefonu
              <b>865009391</b>
              arba rašykite el. p.
              <b>
                <a href="mailto:info@skypark.lt">info@skypark.lt</a>
              </b>
              (I-IV – 12.00-22.00 val., V-VII – 10.00-22.00 val.)
            </p>
            <p>
              <b>Su dienos bilietu</b>
              galite naudotis visomis Skypark teikiamomis paslaugomis, įskaitant žaidimų aikštelę,
              nemokamus žaidimo aparatus, batutų zoną.
            </p>
            <p>
              <b>Šeimos bilietas</b>
              yra skirtas nedaugiau kaip keturiems šeimos nariams.
            </p>
          </div>
        </div>
      </div>
    </section>

  </main>

</div>

<?php include './partials/_foot.php';
