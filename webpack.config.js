/* eslint global-require: 0 */
const path = require('path');
// const pkg = require('./package.json');
// const getPath = require('./config/utils/getPath');

const processScripts = require('./config/processScripts');
const setupDevServer = require('./config/setupDevServer');
const processStyles = require('./config/processStyles');
const processImages = require('./config/processImages');
const processFonts = require('./config/processFonts');
const processTwig = require('./config/processTwig');
const generatePlugins = require('./config/generatePlugins');

module.exports = function generateConfig(env) {
  return {
    entry: {
      app: path.resolve(__dirname, './src/ts/index.ts'),
      // app: path.resolve(__dirname, './src/js/index.js'),
      // app: [
      //   path.resolve(__dirname, './src/ts/index.ts'),
      //   path.resolve(__dirname, './src/ts/work.ts'),
      // ],
    },
    output: {
      // path: path.resolve(__dirname, './dist/'),
      path: path.resolve(__dirname, './'),
      publicPath: './',
      filename: `assets/js/${env.mode === 'legacy' ? 'app--legacy' : 'app'}.js`,
    },
    module: {
      rules: [
        processTwig(env.mode),
        processFonts(env.mode),
        processImages(env.mode),
        processStyles(env.mode),
        ...processScripts(env.mode),
      ],
    },
    plugins: generatePlugins(env.mode),
    devServer: setupDevServer(env.mode),
    devtool: 'source-map',
    mode: env.mode === 'development' ? 'development' : 'production',
    watch: env.mode === 'development',
    watchOptions: {
      ignored: /node_modules/,
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js', '.json'],
    },
  };
};
