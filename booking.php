<?php include './partials/_head.php'; ?>

<div class="app__page app__page--default default">

  <div class="default__intro">

    <header class="default__header header header--white">
      <?php include './partials/header--white.php'; ?>
    </header>

    <?php $heading = 'BOOK A ROOM'; include './partials/scenes/default__scene--4.php'; ?>

  </div>

  <main class="default__content default__content--booking">

    <header class="default__header header header--black">
      <?php include './partials/header--black.php'; ?>
    </header>

    <section class="default__section default__section--booking">
      <div class="default__container default__container--booking container">
        <div class="default__booking booking">
          <div class="booking__intro _before-tween" data-tweener>
            <p>Rooms are for 15-20 persons. In room price is included: lorem ipsum dolor sit amet,
              consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua ea commodo consequat.</p>
          </div>
          <div class="booking__tabs" data-tabs="booking">
            <div class="booking__plans _before-tween" data-tweener data-tweener-suspend="300">
              <div class="booking__grab grab">
                <div class="grab__icon">
                  <?php include './assets/svg/grab__hand.svg'; ?>
                </div>
                <div class="grab__label">Slide to the side</div>
              </div>
              <div class="booking__scroll">
                <div class="booking__plan is-active" data-tabs-content="1">
                  <img alt="" class="booking__image" loading="lazy"
                    src="./media/skypark_plan_first_floor.png" />
                </div>
                <div class="booking__plan" data-tabs-content="2">
                  <img alt="" class="booking__image" loading="lazy"
                    src="./media/skypark_plan_second_floor_2.png" />
                </div>
              </div>
            </div>
            <div class="booking__footer">
              <div class="booking__floors _before-tween" data-tweener>Choose floor</div>
              <div class="booking__bullet is-active _before-tween" data-tweener data-tabs-trigger="1">1</div>
              <div class="booking__bullet _before-tween" data-tweener data-tabs-trigger="2">2</div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="default__section default__section--timetable">
      <div class="default__container default__container--timetable container">
        <div class="default__timetable timetable" data-timetable>
          <div class="timetable__grab grab">
            <div class="grab__icon">
              <?php include './assets/svg/grab__hand.svg'; ?>
            </div>
            <div class="grab__label">Slide to the side</div>
          </div>
          <div class="timetable__scroll">
            <div class="timetable__content">
              <div class="timetable__top _before-tween" data-tweener>
                <div class="timetable__select">
                  <span class="timetable__select-label">Choose week</span>
                  <span class="timetable__dropdown">
                    <select class="timetable__choices" data-timetable-choices>
                      <option value="/query1">04.22 - 04.28</option>
                      <option value="/query2">04.29 - 05.04</option>
                    </select>
                  </span>
                </div>
                <div class="timetable__actions">
                  <span class="timetable__nav timetable__nav--prev timetable__nav--disabled">
                    <i class="timetable__icon timetable__icon--prev">
                      <?php include './assets/svg/arrow--left.svg'; ?>
                    </i>
                    <span class="timetable__nav-label timetable__nav-label--prev">Previous
                      week</span>
                  </span>
                  <a href="/" class="timetable__nav timetable__nav--next">
                    <span class="timetable__nav-label timetable__nav-label--next">Next week</span>
                    <i class="timetable__icon timetable__icon--next">
                      <?php include './assets/svg/arrow--right.svg'; ?>
                    </i>
                  </a>
                </div>
              </div>
              <div class="timetable__table">
                <div class="timetable__row timetable__row--head _before-tween" data-tweener>
                  <div class="timetable__col timetable__col--head timetable__col--info"></div>
                  <div class="timetable__col timetable__col--head timetable__col--dark">
                    <div class="timetable__date">04.22</div>
                    <div class="timetable__weekday">MONDAY</div>
                  </div>
                  <div class="timetable__col timetable__col--head">
                    <div class="timetable__date">04.23</div>
                    <div class="timetable__weekday">TUESDAY</div>
                  </div>
                  <div class="timetable__col timetable__col--head timetable__col--dark">
                    <div class="timetable__date">04.24</div>
                    <div class="timetable__weekday">WEDNESDAY</div>
                  </div>
                  <div class="timetable__col timetable__col--head">
                    <div class="timetable__date">04.25</div>
                    <div class="timetable__weekday">THURSDAY</div>
                  </div>
                  <div class="timetable__col timetable__col--head timetable__col--dark">
                    <div class="timetable__date">04.26</div>
                    <div class="timetable__weekday">FRIDAY</div>
                  </div>
                  <div class="timetable__col timetable__col--head">
                    <div class="timetable__date">04.27</div>
                    <div class="timetable__weekday">SATURDAY</div>
                  </div>
                  <div class="timetable__col timetable__col--head timetable__col--dark">
                    <div class="timetable__date">04.28</div>
                    <div class="timetable__weekday">SUNDAY</div>
                  </div>
                </div>
                <div class="timetable__row _before-tween" data-tweener>
                  <div class="timetable__col timetable__col--info">
                    <div class="timetable__info" data-tweener="scene">
                      <div class="timetable__heading heading" data-tweener-heading>PASAKA</div>
                      <div class="timetable__pricing">M-F 60€ / Sat-s 85€</div>
                      <div class="timetable__more" data-modal-trigger="room1">
                        <i class="timetable__more-icon">
                          <?php include './assets/svg/plus.svg'; ?>
                        </i>
                        <span class="timetable__more-label">DAUGIAU</span>
                      </div>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room1"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                </div>
                <div class="timetable__row _before-tween" data-tweener>
                  <div class="timetable__col timetable__col--info">
                    <div class="timetable__info" data-tweener="scene">
                      <div class="timetable__heading heading" data-tweener-heading>JŪRA</div>
                      <div class="timetable__pricing">M-F 60€ / Sat-s 85€</div>
                      <div class="timetable__more" data-modal-trigger="room1">
                        <i class="timetable__more-icon">
                          <?php include './assets/svg/plus.svg'; ?>
                        </i>
                        <span class="timetable__more-label">DAUGIAU</span>
                      </div>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room2"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                </div>
                <div class="timetable__row _before-tween" data-tweener>
                  <div class="timetable__col timetable__col--info">
                    <div class="timetable__info" data-tweener="scene">
                      <div class="timetable__heading heading" data-tweener-heading>PASAKA</div>
                      <div class="timetable__pricing">M-F 60€ / Sat-s 85€</div>
                      <div class="timetable__more" data-modal-trigger="room1">
                        <i class="timetable__more-icon">
                          <?php include './assets/svg/plus.svg'; ?>
                        </i>
                        <span class="timetable__more-label">DAUGIAU</span>
                      </div>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room3"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                </div>
                <div class="timetable__row _before-tween" data-tweener>
                  <div class="timetable__col timetable__col--info">
                    <div class="timetable__info" data-tweener="scene">
                      <div class="timetable__heading heading" data-tweener-heading>JŪRA</div>
                      <div class="timetable__pricing">M-F 60€ / Sat-s 85€</div>
                      <div class="timetable__more" data-modal-trigger="room1">
                        <i class="timetable__more-icon">
                          <?php include './assets/svg/plus.svg'; ?>
                        </i>
                        <span class="timetable__more-label">DAUGIAU</span>
                      </div>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                  <div class="timetable__col">
                    <div class="timetable__times">
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                      <span class="timetable__time timetable__time--disabled">11:00 - 12:00</span>
                      <span data-modal-trigger="wizard" data-wizard-room="room4"
                        data-wizard-time="123123" data-wizard-price="15"
                        class="timetable__time">11:00
                        - 12:00</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </main>

</div>

<?php include './partials/modals/more.php'; ?>
<?php include './partials/modals/wizard.php'; ?>

<?php include './partials/_foot.php';
