<?php include './partials/_head.php'; ?>

<div class="app__page app__page--room room">

  <div class="room__gallery gallery" data-slider="gallery">

    <header class="room__header header header--white">
      <?php include './partials/header--white.php'; ?>
    </header>

    <div class="gallery__slider swiper-container" data-slider-target>
      <div class="gallery__wrapper swiper-wrapper">
        <div class="gallery__slide swiper-slide">
          <div class="gallery__media media">
            <img alt="" class="media__image media__image--gallery" loading="lazy"
              src="./media/gallery__slide.png" />
          </div>
        </div>
        <div class="gallery__slide swiper-slide">
          <div class="gallery__media media">
            <img alt="" class="media__image media__image--gallery" loading="lazy"
              src="./media/safety--1.png" />
          </div>
        </div>
        <div class="gallery__slide swiper-slide">
          <div class="gallery__media media">
            <img alt="" class="media__image media__image--gallery" loading="lazy"
              src="./media/safety--2.png" />
          </div>
        </div>
      </div>
      <div class="gallery__bullets" data-slider-pagination></div>
      <div class="gallery__bullets">
        <div class="gallery__bullet" data-slider-bullet></div>
        <div class="gallery__bullet" data-slider-bullet></div>
        <div class="gallery__bullet" data-slider-bullet></div>
      </div>
    </div>

  </div>

  <div class="room__wrapper">

    <header class="room__header header header--black">
      <?php include './partials/header--black.php'; ?>
    </header>

    <main class="room__content">

      <div class="room__title _before-tween" data-tweener="scene">
        <h1 data-tweener-heading>PASAKA</h1>
      </div>

      <div class="room__text _before-tween" data-tweener data-tweener-suspend="400">
        <p>Hind 2,5 tunniks:<br>
          <b>E-N 60 € / R-P 85 €</b>
        </p>
        <p>Į kainą įskaičiuota:</p>
        <ul>
          <li>15 šokinėjimo apyrankių (už kiekvieną papildomą šokinėjantį asmenį reikia sumokėti
            pusė dienos bilieto kainos)</li>
          <li>15 vienkartinių indų/įrankių/stiklinių</li>
        </ul>
        <p>Šventės trukmė:<br>
          <b>2.5 val.</b>
        </p>
      </div>

    </main>

  </div>

</div>

<?php include './partials/_foot.php';
