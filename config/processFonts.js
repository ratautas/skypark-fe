module.exports = function processFonts() {
  return {
    test: /\.ttf|eot|woff|woff2$/i,
    use: [{
      loader: 'file-loader',
      options: {
        name: 'assets/fonts/[name].[ext]',
        publicPath: '../../',
      },
    }],
  };
};
