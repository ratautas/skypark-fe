/* eslint global-require: 0 */

module.exports = function processImages(mode) {
  return {
    // test: /\.(png|jpe?g|gif|svg|webp)$/i,
    test: /\.(png|jpe?g|gif|webp)$/i,
    exclude: /(node_modules|bower_components)/,
    use: [{
      loader: 'file-loader',
      options: {
        name: 'assets/img/[name].[ext]',
        publicPath: '../../',
        // publicPath: (url, resourcePath, context) => {
        //   console.log(url);
        //   return '../../' + url
        //   // publicPath is the relative path of the resource to the context
        //   // e.g. for ./css/admin/main.css the publicPath will be ../../
        //   // while for ./css/main.css the publicPath will be ../
        //   // return path.relative(path.dirname(resourcePath), context) + '/';
        // },
        outputPath: './',
      },
    },
    {
      loader: 'img-loader',
      options: {
        plugins: [
          require('imagemin-gifsicle')({
            interlaced: true,
          }),
          require('imagemin-mozjpeg')({
            progressive: true,
            arithmetic: false,
          }),
          require('imagemin-optipng')({
            optimizationLevel: 5,
          }),
        ],
      },
    },
    ],
  };
};
