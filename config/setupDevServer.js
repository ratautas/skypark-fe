const sane = require('sane');
const ENV = require('dotenv').config({
  path: '../../.env',
}).parsed;

const PORT = 9000;
const URL = ENV ? ENV.APP_URL : '';
// const HOST = URL.length ? URL.replace('http://', '') : 'localhost';
const HOST = URL.length ? URL.replace('http://', '') : '0.0.0.0';

module.exports = function setupDevServer() {
  return {
    overlay: true,
    before(app, server) {
      sane('./', {
        glob: ['**/*.htm'],
        ignored: /(node_modules|bower_components)/,
      }).on('change', () => server.sockWrite(server.sockets, 'content-changed'));
    },

    // publicPath: URL ? `${URL}:${PORT}` : '/',
    publicPath: URL,

    // hot: true,
    hot: false,
    // writeToDisk: true,

    host: HOST,
    port: PORT,
    proxy: {
      '/': URL,
    },
    disableHostCheck: true,
    // stats: {
    //   chunks: false,
    // },
    stats: {
      assets: false,
      children: false,
      chunks: false,
      hash: false,
      modules: false,
      publicPath: false,
      timings: false,
      version: false,
      warnings: true,
      colors: {
        green: '\u001b[32m',
      },
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  };
};
