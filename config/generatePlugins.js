/* eslint-disable max-len */
/* eslint-disable global-require */
/* eslint-disable import/no-extraneous-dependencies */
// const fs = require('fs');
const glob = require('glob');
const path = require('path');

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const WriteFileWebpackPlugin = require('write-file-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const WebappWebpackPlugin = require('webapp-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const ImageminWebpackPlugin = require('imagemin-webpack-plugin').default;
const ImageminWebpWebpackPlugin = require('imagemin-webp-webpack-plugin');
const HtmlBeautifyPlugin = require('html-beautify-webpack-plugin');
// const {
//   BundleAnalyzerPlugin,
// } = require('webpack-bundle-analyzer');

const CopyWebpackPlugin = require('copy-webpack-plugin');
// const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin');

const pkg = require('../package.json');
const hasJquery = require('./utils/hasJquery');

const twigTemplates = glob.sync('./src/twig/*.twig').map(
  (file) => new HtmlWebpackPlugin({
    cache: true,
    template: file,
    title: `${path.basename(file).replace('.twig', '')} template`, // page title
    filename: path.basename(file).replace('.twig', '.html'), // Output
  }),
);


module.exports = function generatePlugins(mode) {
  const plugins = [
    ...twigTemplates,

    new HardSourceWebpackPlugin(),

    new MiniCssExtractPlugin({
      filename: `assets/css/${mode === 'legacy' ? 'app--legacy' : 'app'}.css`,
    }),

    // new HtmlReplaceWebpackPlugin([{
    //   pattern: '../media/',
    //   replacement: 'media/'
    // }]),

    new CopyWebpackPlugin([
      // hacky way to produce webp:
      {
        cache: true,
        from: './src/img/**/*',
        to: '../',
        ignore: ['*.svg', '*.webp', 'favicon.png'],
      },
      {
        cache: true,
        from: './src/svg/**/*',
        to: './assets/img',
        transformPath: (targetPath) => targetPath.replace('src/svg/', ''),
      },
      {
        cache: true,
        from: './src/media/**/*',
        to: './media',
        transformPath: (targetPath) => targetPath.replace('src/media/', ''),
      },
    ]),


    new ImageminWebpackPlugin({
      config: [{
        test: /\.(jpe?g|png|gif|webp)$/i,
        options: {
          plugins: [
            require('imagemin-gifsicle')({
              interlaced: true,
              optimizationLevel: 3,
            }),
            require('imagemin-mozjpeg')({
              quality: 65,
              progressive: true,
            }),
            require('imagemin-optipng')({
              interlaced: true,
              optimizationLevel: 3,
            }),
            // require('imagemin-svgo')({
            //   plugins: [{
            //       convertPathData: false
            //     }, {
            //       cleanupIDs: false
            //     }
            //   ]
            // }),
          ],
        },
      }],
    }),

    new ImageminWebpWebpackPlugin(),

  ];

  if (twigTemplates.length) {
    plugins.push(
      new HtmlBeautifyPlugin({
        config: {
          html: {
            indent_size: 2,
            end_with_newline: true,
            indent_inner_html: true,
            preserve_newlines: false,
            wrap_line_length: 0,
          },
        },
      }),
    );
  }

  if (mode === 'development') {
    plugins.push(
      new StyleLintPlugin({
        context: path.resolve(__dirname, '../src/'),
      }),
      new WriteFileWebpackPlugin(),
    );
  } else {
    plugins.push(
      new WebappWebpackPlugin({
        cache: true,
        prefix: 'assets/img/webapp/',
        logo: './src/img/favicon.png',
        publicPath: '../../',
        favicons: {
          appName: pkg.name || 'app',
          appDescription: pkg.description || 'App Description',
          developerName: pkg.author || 'Me',
          background: pkg.background || '#ddd',
          theme_color: pkg.color || '#333',
          start_url: '/',
          // icons: {
          //   android: false, // Create Android homescreen icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   appleIcon: false, // Create Apple touch icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   appleStartup: false, // Create Apple startup images. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   coast: false, // Create Opera Coast icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   favicons: true, // Create regular favicons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   firefox: false, // Create Firefox OS icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   windows: false, // Create Windows 8 tile icons. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          //   yandex: false // Create Yandex browser icon. `boolean` or `{ offset, background, mask, overlayGlow, overlayShadow }`
          // }
        },
      }),

      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          map: {
            inline: false,
            annotation: true,
          },
        },
      }),

      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: true,
      }),

      // new BundleAnalyzerPlugin(),

      // new CompressionPlugin({
      //   test: /\.(css|js|svg|ttf|eot|woff|woff2)$/
      // }),
      // new CleanWebpackPlugin(),
      // new WorkboxWebpackPlugin.GenerateSW({
      //   // these options encourage the ServiceWorkers to get in there fast
      //   // and not allow any straggling "old" SWs to hang around
      //   clientsClaim: true,
      //   skipWaiting: true
      // })
    );
    if (hasJquery()) {
      plugins.push(new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
      }));
    }
  }
  return plugins;
};
