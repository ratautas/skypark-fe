module.exports = function processFonts() {
  return {
    test: /\.twig$/,
    use: [{
      loader: 'twig-loader',
    }],
  };
};
